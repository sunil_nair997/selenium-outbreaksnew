package com.baxter.seleniumopat.suites;

import com.baxter.seleniumopat.tests.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        WorkflowInitialDashboardTest.class,
        WorkflowAddOtherPatientsTest.class,
        WorkflowAddFullPatientTest.class,
        WorkflowEditPatientTest.class,
        WorkflowAddPatientReactionsTest.class,
        WorkflowEndEpisodeTest.class,
        WorkflowAddMedPatientDetailTest.class,
        WorkflowEndMedPatientEpisodeTest.class,
        WorkflowAddOnlyPatientDetailTest.class,
        WorkflowEditPatientOutTest.class,
        WorkflowEndOnlyPatientEpisodeTest.class
})

public class StorySuite {

}
