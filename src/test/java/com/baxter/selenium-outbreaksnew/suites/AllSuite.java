package com.baxter.seleniumopat.suites;

import com.baxter.seleniumopat.tests.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DashboardTest.class,
        ListCandidatesTest.class,
        ListPatientsTest.class,
        ListChecksTest.class,
        ListEpisodesTest.class,
        ListMedicationsTest.class,
        ListReactionsTest.class,
        ListTimelineTest.class,
        SummaryTest.class,
        AddCheckTest.class,
        AddEpisodeTest.class,
        AddMedicationTest.class,
        AddNoteTest.class,
        AddReactionTest.class,
        AddReportTest.class,
        AddSettingsTest.class,
        CancelFutureEpisodeTest.class,
        EditEpisodeTest.class,
        EditMedicationTest.class,
        EditPatientTest.class,
        EditReactionTest.class,
        EndEpisodeTest.class,
        ShowEpisodeTest.class
})

public class AllSuite {

}
