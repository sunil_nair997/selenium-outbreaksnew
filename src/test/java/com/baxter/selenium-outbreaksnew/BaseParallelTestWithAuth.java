package com.baxter.seleniumopat;

import com.baxter.seleniumopat.pages.LoginPage;
import org.junit.*;

import static org.junit.Assert.assertTrue;

/**
 * Base test class which authenticate default user before tests
 *
 */
abstract public class BaseParallelTestWithAuth extends BaseParallelTest {

    // constructor
    public BaseParallelTestWithAuth(BrowserTarget browserTarget) {
        super(browserTarget);
    }

    /**
     * initialise pages and login
     */
    @Before
    public void setUpBeforeTests() {
       new LoginPage(driver).login(config.getProperty("username"), config.getProperty("password"));
    }

    /**
     * perform logout and quit driver, so any busy grid nodes can be reused(if using selenium-grid)
     */
    @After
    public void tearDownAfterTestsBaseClassWithAuth() {

        formHandler = null;
    }
}
