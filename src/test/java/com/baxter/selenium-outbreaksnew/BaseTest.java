package com.baxter.seleniumopat;

import com.baxter.seleniumopat.utilities.*;
import com.baxter.seleniumopat.utilities.driver.WaitingRemoteWebDriver;
import com.baxter.seleniumopat.utilities.rules.TestPrintHandler;
import org.junit.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.management.InstanceAlreadyExistsException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.*;

/**
 * @deprecated replaced by BaseParallelTest
 */
abstract public class BaseTest {

    /**
     * Configuration needed for the tests
     */
    protected static Config config;

    /**
     * Logger
     */
    protected static Logger logger;

    /**
     * Driver used for test
     */
    protected static WaitingRemoteWebDriver driver;

    protected static FormHandler formHandler;

    /**
     * This rule intercepts failed unit tests before JUnit sends out the
     * results. The idea is that the callback TestPrintHandler can extract
     * only useful information from the exception and export it to the logs.
     */
    @Rule
    public TestPrintHandler failure = new TestPrintHandler(logger, config);

    @BeforeClass
    public static void setUpClass() throws MalformedURLException {

        // initialize log
        initLogger();

        // load configurations needed for the tests
        initConfig();

        RemoteWebDriver remoteWebDriver = null;

        //  run headless tests
        if (config.getProperty("headless-mode").equals("true")) {

            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--start-maximized");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--headless");

            // pointing to selenium grid
            remoteWebDriver = new RemoteWebDriver(new URL("http://ci-devops.icnetplc.com:4444/wd/hub"), chromeOptions);
        }
        else {

            remoteWebDriver = new ChromeDriver();
        }


        setDriver(new WaitingRemoteWebDriver(remoteWebDriver, 10));

        String baseUrl = config.getProperty("baseurl");
        driver.setBaseUrl(baseUrl);
        driver.manage().window().maximize();
    }

    /**
     * Setter for injecting driver if needed
     *
     * @param _driver
     * @throws InstanceAlreadyExistsException
     */
    public static void setDriver(WaitingRemoteWebDriver _driver) {
        initLogger();
        initConfig();
        driver = _driver;
        initFormHandler();
    }

    private static void initConfig() {
        if (config == null) {
            config = new Config();
            String driverPath = config.getProperty("driverPath");
            if (driverPath != null && !driverPath.equals("")) {
                System.setProperty("webdriver.chrome.driver", driverPath);
            }
        }
    }

    private static Logger initLogger() {
        if (logger == null) {
            logger = new TestLogger(BaseTest.class.getSimpleName()).get();
        }

        return logger;
    }

    private static FormHandler initFormHandler() {
        if (formHandler == null) {
            formHandler = new FormHandler(driver);
        }

        return formHandler;
    }

    @AfterClass
    public static void tearDownClass() {

        driver.close();
        driver.quit();
        config = null;
        formHandler = null;
    }
}
