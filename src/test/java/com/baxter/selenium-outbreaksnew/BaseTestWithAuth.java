package com.baxter.seleniumopat;

import org.junit.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertTrue;

/**
 * Base test class which authenticate default user before tests
 *
 */
abstract public class BaseTestWithAuth extends BaseTest {

    /**
     * perform login
     */
    @BeforeClass
    public static void authenticateUserFromConfig() {
        /*
        new LoginPage(driver).login(
            config.getProperty("username"),
            config.getProperty("password")
        );
        */
    }

    /**
     * Log tests have ended and perform logout
     */
    @AfterClass
    public static void logoutUser() {

        //new LogoutPage(driver).logout();
    }

    @Before
    public void clearAllAlerts() {
        if (driver.isAlertPresent()) {
            Alert alert = ExpectedConditions.alertIsPresent().apply(driver);
            alert.dismiss();
        }
    }
}
