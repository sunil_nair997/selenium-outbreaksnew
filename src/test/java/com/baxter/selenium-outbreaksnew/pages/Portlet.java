package com.baxter.seleniumopat.pages;

import com.baxter.seleniumopat.utilities.ContentHandler;
import org.apache.commons.lang.ArrayUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class Portlet {
    private static final String TITLE_CLASS = "portlet-title";
    private static final String BODY_CLASS = "portlet-body";
    private static final String EMPTY_TEXT_CLASS = "text-center";

    WebElement portlet;
    private WebElement portletTable;
    private List<WebElement> tableRows;

    String expectedEmptyText;
    String expectedHeadings[];
    String expectedTitle;

    private String titleText;
    private String emptyText;
    private boolean emptyPortlet;
    private int rowCount = 0;

    public Portlet(WebElement _portlet) {
        this.portlet = _portlet;

        assertTrue("Portlet not displayed",_portlet.isDisplayed());
        titleText = portlet.findElement(By.className(TITLE_CLASS)).getText();
        System.out.println("Portlet title: " + titleText);

        WebElement portletBody = portlet.findElement(By.className(BODY_CLASS));
        List<WebElement> portletContent = portletBody.findElements(By.className(EMPTY_TEXT_CLASS));
        switch (portletContent.size()) {
            case 0:
                emptyPortlet = false;
                List<WebElement>portletTables = portletBody.findElements(By.tagName("table"));
                assertEquals("Malformed portlet " + titleText +  ", table mismatch: ", 1, portletTables.size());

                // Expand table if needed
                List<WebElement>tableLengthControls = portlet.findElements(By.cssSelector(ContentHandler.TABLE_LENGTH_CONTROL_CSS));
                if (tableLengthControls.size() > 1)
                    fail("Multiple table size controls found");
                else if (tableLengthControls.size() > 0) {
                    Select selectTableLength = new Select(tableLengthControls.get(0));
                    selectTableLength.selectByValue(ContentHandler.TABLE_LENGTH_MAX);
                }

                portletTable = portletTables.get(0);
                tableRows = portletTable.findElements(By.tagName("tr"));
                rowCount = tableRows.size() - 1; // ignore heading
                break;
            case 1:
                emptyPortlet = true;
                emptyText = portletContent.get(0).getText();
                break;
            default:
                fail("Malformed portlet (body missing): " + titleText);
        }
    }

    public void setExpectedTitle(String _expectedTitle){
        this.expectedTitle = _expectedTitle;
    }

    public void setExpectedEmptyText(String _expectedEmptyText){
        this.expectedEmptyText = _expectedEmptyText;
    }

    public void setExpectedTableHeadings(String _expectedHeadings[]){
        this.expectedHeadings = _expectedHeadings;
    }

    public void checkEmptyPortlet(){
        assertEquals("Portlet title mismatch: " + expectedTitle, expectedTitle, titleText);
        assertTrue("Portlet " + expectedTitle + " not empty", emptyPortlet);
        assertEquals("Portlet empty text mismatch: " + expectedEmptyText, expectedEmptyText, emptyText);
    }

    /**
     * check the data in the portlet
     * @param _expectedData data to check; needs to match a row exactly
     * @param _exactMatch true = no other rows exist, false = check for the existence of
     *                    _expectedData, other data can be present
     */
    public void checkPortlet(String _expectedData[][], boolean _exactMatch)
    {
        int numRowsToCheck = _expectedData.length,
            numExpectedColumns = _expectedData[0].length,
            colIndex;

        String columnHeadingText;
        String portletTableData[][];

        assertEquals("Portal title mis-match. Expected: " + expectedTitle + " Actual: " + titleText, expectedTitle, titleText);
        if (_exactMatch) {
            assertEquals(expectedTitle + " portal table rows mis-match: ", numRowsToCheck, rowCount);
        }

        // Check heading
        List<WebElement>columnHeadings = tableRows.get(0).findElements(By.tagName("th"));
        assertEquals(expectedTitle + " portlet table columns mis-match: ",
                expectedHeadings.length, columnHeadings.size());
        for (WebElement columnHeading:columnHeadings) {
            columnHeadingText = columnHeading.getText();
            assertTrue("Column Heading mis-match: "+columnHeadingText, ArrayUtils.contains(expectedHeadings,columnHeadingText));
        }

        List<WebElement>tableDatums;
        // put portal Table into and array
        portletTableData = new String[rowCount][numExpectedColumns];
        for (int i = 1; i <= rowCount; i++) {
            tableDatums = tableRows.get(i).findElements(By.tagName("td"));
            colIndex = 0;
            for (WebElement tableDatum:tableDatums) {
                portletTableData[i-1][colIndex] = tableDatum.getText();
                colIndex++;
            }
        }

        // check expected Rows are in the table
        assertTrue("Failed to find data in table",ContentHandler.checkArrays(_expectedData, portletTableData));
    }

}
