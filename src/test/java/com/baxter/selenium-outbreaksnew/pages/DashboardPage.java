package com.baxter.seleniumopat.pages;

import com.baxter.seleniumopat.utilities.LayoutHandler;
import com.baxter.seleniumopat.utilities.driver.WaitingRemoteWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.text.DecimalFormat;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DashboardPage {
    private WaitingRemoteWebDriver driver;
    private LayoutHandler layoutHandler;
    private Logger logger;

    private int activeOutbreaks = 0;
    private int newOutbreaks = 0;
    private int peopleInOutbreaks = 0;
    private int outbreakDeaths = 0;

    public static final String LINK_ID = "text-left";

    public static final String ACTIVE_OUTBREAKS_COUNT_ID = "";
    public static final String NEW_OUTBREAKS_COUNT_ID = "";
    public static final String PEOPLE_IN_OUTBREAKS_COUNT_ID = "";
    public static final String ACTIVE_DEATHS = "";

    public static final String INCIDENCE_PANEL_ID = "";
    public static final String INCIDENCE_ID = "";

    public static final String MAP_PANEL_ID = "";
    public static final String MAP_ID = "";

    public static final String ALL_OUTBREAKS_MENU = "";
    public static final String OUTBREAKS_LIST_MENU = "";
    public static final String ADMIN_MENU = "";
    public static final String LINK_OUTBREAKS_MENU = "";
    public static final String MERGE_OUTBREAKS_MENU = "";
    public static final String CASE_DEFINITIONS_MENU = "";
    public static final String LOCATIONS_MENU = "";
    public static final String AUDIT_MENU = "";

    public static final String NEW_OUTBREAK_ACTION_MENU = "";

    public static final String[] MENU_ITEM_IDS = {
            ALL_OUTBREAKS_MENU,
            OUTBREAKS_LIST_MENU,
            ADMIN_MENU,
            LINK_OUTBREAKS_MENU,
            MERGE_OUTBREAKS_MENU,
            CASE_DEFINITIONS_MENU,
            LOCATIONS_MENU,
            AUDIT_MENU,
            NEW_OUTBREAK_ACTION_MENU
    };

    public static final String PAGE_TITLE = "Outbreaks";

    @FindBy (id=ALL_OUTBREAKS_MENU)
    public WebElement homeMenu;

    @FindBy (id=ACTIVE_OUTBREAKS_COUNT_ID)
    public WebElement countActiveOutbreaks;
    @FindBy (id=NEW_OUTBREAKS_COUNT_ID)
    public WebElement countNewOutbreaks;
    @FindBy (id=PEOPLE_IN_OUTBREAKS_COUNT_ID)
    public WebElement countPeopleInOutbreaks;
    @FindBy (id=ACTIVE_DEATHS)
    public WebElement countDeaths;


    public DashboardPage(WaitingRemoteWebDriver _driver, Logger _logger) {

        this.driver = _driver;

        PageFactory.initElements(driver, this);

        driver.get(driver.getBaseUrl());
        this.logger = _logger;
    }

    /**
     * Sets the data on the object for the panels
     */
    public void setCheckpoint() {
        checkpointActiveOutbreaks = Integer.parseInt(countActiveOutbreaks.getText());
        checkpointNewOutbreaks = Integer.parseInt(countNewOutbreaks.getText());
        checkpointPeopleInOutbreaks = Integer.parseInt(countPeopleInOutbreaks.getText());
        checkpointDeathCount = Integer.parseInt(countDeaths.getText());
    }

    /**
     * Checks the data on the dashboard widgets against the stored
     * data in the object (setCheckpoint) with a difference expected
     * for the panels
     *
     * @param compActiveOutbreaks expected number of Active Outbreaks
     * @param compNewOutbreaks expected number of New Outbreaks
     * @param compPeopleInOutbreaks expected number of People in active outbreaks
     * @param compDeaths expected number of Deaths in active outbreaks
     *
     */

    public void compareCheckpoint(int compActiveOutbreaks,
                                  int compNewOutbreaks,
                                  int compPeopleInOutbreaks,
                                  int compDeaths) {
        DecimalFormat formatter;

        assertEquals("Mismatch in Number of Active Outbreaks :",
                checkpointActiveOutbreaks +compActiveOutbreaks,
                Integer.parseInt(countActiveOutbreaks.getText()));
        assertEquals("Mismatch in NUmber of New Outbreaks :",
                checkpointNewOutbreaks +compNewOutbreaks,
                Integer.parseInt(countNewOutbreaks.getText()));
        assertEquals("Mismatch in People in Outbreaks :",
                checkpointPeopleInOutbreaks +compPeopleInOutbreaks,
                Integer.parseInt(countPeopleInOutbreaks.getText()));
        assertEquals("Mismatch in Daeths in Outbreaks :",
                checkpointDeathCount +compDeaths,
                Integer.parseInt(countDeaths.getText()));

    }

    public void checkPageLayout() {
        System.out.println("*** Checking Dashboard Page layout ***");
        logger.fine("*** Checking Dashboard Page layout ***");

        layoutHandler = new LayoutHandler(driver);
        layoutHandler.checkPageLayout(PAGE_TITLE, MENU_ITEM_IDS,true,false);
        assertTrue("Google map missing from dashboard",layoutHandler.isIdPresent(MAP_ID));
    }

    // This can be called from outside the class to check the page without instantiating it (login)
    public static void checkPageLayout(WaitingRemoteWebDriver _driver) {
        LayoutHandler layoutHandler = new LayoutHandler(_driver);
        layoutHandler.checkPageLayout(PAGE_TITLE, MENU_ITEM_IDS,true,false);
        assertTrue("Map missing from dashboard",layoutHandler.isIdPresent(MAP_ID));
    }

    public void checkPageLayout(boolean _mapPresent,
                                String _expectedActiveOutbreaks,
                                String _expectedNewOutbreaks,
                                String _expectedPeopleInOutbreaks,
                                String _expectedDeathsInOutbreaks) {
        logger.fine("*** Checking Dashboard Page layout and content ***");

        layoutHandler = new LayoutHandler(driver);
        layoutHandler.checkPageLayout(PAGE_TITLE, MENU_ITEM_IDS,true,false);

        if (_expectedActiveOutbreaks.equals("")) {
            logger.fine("Ignoring Active Outbreaks");
        } else {
            assertEquals("Active Outbreak count mis-match: ",
                    _expectedActiveOutbreaks,
                    countActiveOutbreaks.getText());
        }
        if (_expectedNewOutbreaks.equals("")) {
            logger.fine("Ignoring New Outbreaks count");
        } else {
            assertEquals("New Outbreaks count mis-match: ",
                    _expectedNewOutbreaks,
                    countNewOutbreaks.getText());
        }
        if (_expectedPeopleInOutbreaks.equals("")) {
            logger.fine("Ignoring People in Outbreaks count");
        } else {
            assertEquals("People in Outbreaks count mis-match: ",
                    _expectedPeopleInOutbreaks,
                    countPeopleInOutbreaks.getText());
        }
        if (_expectedDeathsInOutbreaks.equals("")) {
            logger.fine("Ignoring Deaths in Outbreaks count");
        } else {
            assertEquals("Deaths in Outbreaks count mis-match: ",
                    _expectedDeathsInOutbreaks,
                    countDeaths.getText());
        }
        if (_mapPresent)
            assertTrue("Map missing from dashboard",layoutHandler.isIdPresent(MAP_ID));
        else
            assertFalse("Found unexpected Map missing from dashboard",layoutHandler.isIdPresent(MAP_ID));
    }

}
