package com.baxter.seleniumopat.pages;

import com.baxter.seleniumopat.utilities.driver.WaitingRemoteWebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    private WaitingRemoteWebDriver driver;

    @FindBy(id = "username")
    private
    WebElement username;

    @FindBy(id = "password")
    private WebElement password;

    public LoginPage(WaitingRemoteWebDriver _driver) {

        this.driver = _driver;
        driver.get(driver.getBaseUrl());

        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPageAfterLogin() {

        /*
         * This sometimes fails with time out - didn't previously happen on the span.
         * Seems to be on the first test of a run, but not always
         */
        WebElement outbreaksID = driver.waitForElementVisible("id=menu-title");
        if (outbreaksID.getText().equals(DashboardPage.PAGE_TITLE)) {
            return true;
        }
        else {
            System.out.println("Outbreaks Page not loaded");
            return false;
        }
    }

    /**
     * Tries to log in
     */
    public boolean login(String _username, String _password) {

        username.sendKeys(_username);
        password.sendKeys(_password);
        password.submit();

        return checkCorrectPageAfterLogin();
    }
}