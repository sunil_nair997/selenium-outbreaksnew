package com.baxter.seleniumopat.utilities.rules;

import com.baxter.seleniumopat.utilities.Config;
import com.baxter.seleniumopat.utilities.driver.WaitingRemoteWebDriver;
import org.apache.commons.io.FileUtils;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The purpose of this class is to intercept exceptions before JUnit sends out
 * the results, and, filter the information that can help identify the issue
 * that have triggered the exception.
 * <p>
 * The handler is declared in the BaseTest class and any test must "throws
 * exception" if test is to be tracked by this class.
 *
 * @see com.baxter.seleniumopat.BaseTest @Rule TestPrintHandler failure
 */
public class TestPrintHandler implements MethodRule {

    /**
     * Logger
     */
    private Logger logger;
    /**
     * WaitingRemoteWebDriver
     */
    private WaitingRemoteWebDriver driver;
    /**
     * Config
     */
    private Config config;

    private FrameworkMethod frameworkMethod;


    /**
     * for logging purpose only
     */
    public TestPrintHandler(Logger _logger, Config _config) {

        // initialize log
        this.logger = _logger;
        this.config = _config;
    }

    public void setDriver(WaitingRemoteWebDriver _driver) {

        this.driver = new DummyDriver(_driver).getDriver();
    }

    public Statement apply(final Statement statement, final FrameworkMethod frameworkMethod, final Object o) {

        this.frameworkMethod = frameworkMethod;

        return new Statement() {

            @Override
            public void evaluate() throws Throwable {

                long timeStart = System.currentTimeMillis();
                try {
                    statement.evaluate();
                    logger.log(Level.INFO, ". PASS " + getTestCaseNameWithDuration(timeStart));
                    terminateGracefully(false);
                } catch (Throwable t) {
                    logger.log(
                        Level.WARNING,
                        "! FAIL "
                            + getTestCaseNameWithDuration(timeStart)
                            + getTestFailureTrace(t)
                    );

                    // exception will be thrown only when a test fails.
                    // rethrow to allow the failure to be reported by JUnit
                    terminateGracefully(true);
                    throw t;
                }
            }

            private void terminateGracefully(boolean hasError) {

                try {
                    if (driver.isAlertPresent()) {
                        Alert alert = driver.switchTo().alert();
                        logger.log(Level.WARNING, alert.getText());
                        alert.dismiss();
                    }
                }
                finally {

                    // take screenshot of the failure and save it to surefire-reports folder
                    if (config.getProperty("screenshot").equals("true") && hasError) {
                        takeScreenshot();
                    }

                    if (!frameworkMethod.getMethod().getName().equals("logoutTest") &&
                            !frameworkMethod.getMethod().getName().equals("loginTest"))
                    {
                        System.out.println(frameworkMethod.getMethod().getName());
                        //new LogoutPage(driver).logout();
                    }

                    String target = config.getProperty("test-targets");
                    config = null;
                    if (driver != null) {
                        driver.close();
                        if (!target.equals("local-firefox.json")) {
                            driver.quit();
                        }
                    }
                }
            }

            private void takeScreenshot() {
                // Take screenshot and store as a file format
                if (driver != null) {

                    File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                    try {
                        // now copy the  screenshot to desired location using copyFile //method
                        String filePathRoot = "target/surefire-reports/screenshots/";
                        String fullFilePath = filePathRoot + "/" + frameworkMethod.getName() + ".png";

                        File directory = new File(filePathRoot);
                        if (!directory.exists()) {
                            directory.mkdir();
                        }
                        FileUtils.copyFile(screenshot, new File(fullFilePath));
                    } catch (IOException e) {
                        logger.log(Level.WARNING, "Error screenshot could not be saved. " + e.getMessage());
                    }
                }
            }

            private String getTestCaseName() {
                StringBuilder sb = new StringBuilder();
                String className = frameworkMethod.getMethod()
                    .getDeclaringClass().getName()
                    .replaceFirst("com.baxter.seleniumopat.tests.", "");

                sb.append(className)
                    .append("::")
                    .append(frameworkMethod.getName());
                return sb.toString();
            }

            private String getTestCaseNameWithDuration(long timeFrom) {
                StringBuilder sb = new StringBuilder();
                sb.append(getTestCaseName())
                    .append(" (")
                    .append(new DecimalFormat("0.000").format(getTimeElapsed(timeFrom)))
                    .append("s)");
                return sb.toString();
            }

            private double getTimeElapsed(long timeFrom) {
                return (System.currentTimeMillis() - timeFrom) / 1000.0;
            }

            /**
             * This method deals with the exception message;
             *
             * As it is, will print useful information like exception type, file
             * that originated the exception, friendly message and possible code
             * lines that triggered the exception;
             *
             * The code currently in this method is for illustration purpose
             * only; TODO implement correct logging procedures for this method
             *
             * @param t
             * @throws Exception
             */
            private String getTestFailureTrace(Throwable t) throws Exception {

                // get the first line of exception message, which is the most friendly one
                StringBuilder sb = new StringBuilder();

                String message = t.getMessage();

                String tab = "\n";

                if (message != null) {
                    String[] exceptionMessages = message.split("\r\n|\r|\n");
                    for (String messageLine : exceptionMessages) {
                        if (messageLine.contains("By.")) {
                            messageLine = messageLine.replace("By.", "\nBy.");
                        }
                        sb.append(tab + messageLine);
                    }
                }

                sb.append(tab + "\nClasses related:");

                // filter the code lines that could possibly have triggered the exception
                StackTraceElement[] stackTraceElements = t.getStackTrace();
                int index = 0;

                int indexWidth = String.valueOf(stackTraceElements.length).length();
                String indexFormat = tab + "%" + (indexWidth + 2) + "s: ";

                for (StackTraceElement element : stackTraceElements) {
                    if (element.toString().contains("com.baxter")) {
                        sb.append(
                            String.format(indexFormat, index)
                                + element.toString().replace("com.baxter.seleniumopat.", "")
                        );
                    }
                    index++;
                }
                return sb.toString();
            }
        };
    }

    class DummyDriver {
        private WaitingRemoteWebDriver driver;

        public DummyDriver(WaitingRemoteWebDriver _driver) {
            this.driver = _driver; // you can access
        }

        public WaitingRemoteWebDriver getDriver() {
            return driver;
        }
    }
}
