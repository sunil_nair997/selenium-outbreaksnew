package com.baxter.seleniumopat.utilities;

import com.baxter.seleniumopat.utilities.driver.WaitingRemoteWebDriver;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * The content handler for OPAT will be used to check content on different pages
 * but in the same format, e.g check a list of patient names in a table where we don't care about the
 * rest of the table (OPAT Candidates and patients)
 */
public class ContentHandler {

    private WaitingRemoteWebDriver driver;

    /**
     * initialise webdriver
     *
     * @param _driver page driver
     */

    public static final String TABLE_LENGTH_CONTROL_CSS = "div[class='dataTables_length'] select[aria-controls='%s']";
    public static final String TABLE_LENGTH_MAX = "100";
    public static final String TABLE_WRAPPER_TAG = "_wrapper";

    public static final String SHOW_TABLE_CSS = "form-group";
    public static final String SHOW_HEADER_TAG = "label";
    public static final String SHOW_DATA_TAG = "div";

    public ContentHandler(WaitingRemoteWebDriver _driver) {
        driver = _driver;
    }

    /**
     *  Checks that all of the rows in _expectedData are matched EXACTLY (content and order)
     *  in _contentData
     *  Does not check extras in _contentData
     * @param _expectedData
     * @param _contentData
     * @return true if all "rows" in _expectedData have a match in _contentData
     */
    public static boolean checkArrays(String[][] _expectedData, String[][] _contentData){
        int expectedRowIndex = 0,
            contentRowIndex;
        boolean rowNotFound;

        do {
            contentRowIndex = 0;
            do {
                rowNotFound = !(Arrays.deepEquals(_expectedData[expectedRowIndex],_contentData[contentRowIndex]));
                contentRowIndex++;
            } while ((contentRowIndex < _contentData.length) && rowNotFound); // exit if found the row or reached limit
            expectedRowIndex++;
        } while ((expectedRowIndex < _expectedData.length) && !rowNotFound); // exit if didn't find or reached limit
        if (rowNotFound) {
            String errMsg="Match not found for: ";
            for (String datum:_expectedData[expectedRowIndex - 1]) {
                errMsg = errMsg +"["+ datum +"]";
            }
            System.out.println(errMsg);
            return false;
        }
        return true;
    }

    public void checkListContent (WebElement _listElement, String[] _expectListItems, boolean _exactMatch) {
        List<WebElement> listItems = _listElement.findElements(By.tagName("li"));
        String currentItem;

        if (_exactMatch) {
            int expected = _expectListItems.length;
            int found = listItems.size();
            assertEquals("List count not equal to expected data. Expected: "+expected+", Found: "+found,expected,found);
        }

        for (WebElement listItem : listItems) {
            currentItem = listItem.getText();
            assertTrue("Data not expected in list: " + currentItem, ArrayUtils.contains(_expectListItems, currentItem));
        }
    }

    /**
     * Checks for the wrapper on the table and expands it
     * @param _tableId
     */
    private void expandTable(String _tableId) {
        //if table in wrapper, expect the size select and set table length to max
        LayoutHandler layoutHandler = new LayoutHandler(driver);
        if (layoutHandler.isIdPresent(_tableId+TABLE_WRAPPER_TAG)) {
            WebElement tableLengthControl = driver.findElement(By.cssSelector(String.format(TABLE_LENGTH_CONTROL_CSS,_tableId)));
            Select selectTableLength = new Select(tableLengthControl);
            selectTableLength.selectByValue(TABLE_LENGTH_MAX);
        }
    }

    /**
     *
     * @param _tableId the Id of the table on the page
     * @param _rowData array of data expected to be found in the table
     * @param _colIndex column to match the data in (1 relative)
     * @param _exactMatch only the exact rows in the table and no extras
     *
     */
    public void checkTableContent (String _tableId, String[] _rowData, int _colIndex, boolean _exactMatch) {
        expandTable(_tableId);
        WebElement dataTable = driver.findElementById(_tableId);
        // Sort type tables need time to display
        driver.waitForElementVisible(dataTable.findElement(By.cssSelector("tbody")));
        List<WebElement> dataTableRows = dataTable.findElements(By.xpath(".//tbody/tr"));
        String columnXpath = "./td["+_colIndex+"]";
        String currentDatum = "";

        if (_exactMatch)
            assertEquals(_tableId + " table row count not equal to expected data.", _rowData.length, dataTableRows.size());

        for (WebElement dataTableRow : dataTableRows) {
            currentDatum = dataTableRow.findElement(By.xpath(columnXpath)).getText();
            assertTrue("Data not expected in table: " + currentDatum, ArrayUtils.contains(_rowData, currentDatum));
        }
    }

    public void checkTableHeader (String _tableId, String[] _headingData) {

        WebElement dataTable = driver.findElementById(_tableId);
        List<WebElement> tableHeader = dataTable.findElements(By.xpath(".//thead/tr/th"));
        String columnText;

        int colCount = tableHeader.size();
        assertEquals(_tableId + " number of columns does not match expected.", _headingData.length, colCount);

        for (WebElement columnHeader : tableHeader) {
            columnText = columnHeader.getText();
            assertTrue("Heading not expected in table: " + columnText, ArrayUtils.contains(_headingData, columnText));
        }
    }

    /**
     * Finds and returns the row for a given text string in a given table on the page
     * @param _tableId the Id of the table on the page
     * @param _rowData string of data expected to be found in the table
     * @return WebElement for the clickable row of the page for the patient
     *  null if not found
     */
    public WebElement findRowInTable(String _tableId, String _rowData) {
        /*
         bug https://bugzilla.mozilla.org/show_bug.cgi?id=1448825 means that using firefox driver you cannot click
         on a table row, so returning the td instead, so that it can be clicked on return.
          */
        expandTable(_tableId);

        WebElement dataTable = driver.findElementById(_tableId);
        // Sort type tables need time to display
        driver.waitForElementVisible(dataTable,"tbody");

        List<WebElement> dataTableRows = dataTable.findElements(By.xpath(".//tr/td[contains(text(), '"+_rowData+"')]"));

        switch (dataTableRows.size()) {
            case 0:
                System.out.println("No data found for: "+_rowData);
                break;
            case 1:
                return dataTableRows.get(0);
            default :
                System.out.println("Multiple rows found, expecting one for: "+_rowData);
        }
        return null;
    }

    /**
     * Finds and returns the row for a given text string in a given table in a given webElement
     * @param _webElement the WebElement (e.g. portlet) that contains the table
     * @param _tableId the Id of the table on the element
     * @param _rowData string of data expected to be found in the table
     * @return WebElement for the clickable row of the page for the patient
     *  null if not found
     */
    public static WebElement findRowInTable(WebElement _webElement, String _tableId, String _rowData) {
        /*
         bug https://bugzilla.mozilla.org/show_bug.cgi?id=1448825 means that using firefox driver you cannot click
         on a table row, so returning the td instead, so that it can be clicked on return.
          */

        // if table is expandable set table length
        // equivalent to expand Table but without driver
        List<WebElement> tableLengthControls = _webElement.findElements(By.cssSelector(String.format(TABLE_LENGTH_CONTROL_CSS, _tableId)));
        if (tableLengthControls.size() > 0) {
            if (tableLengthControls.size() > 1)
                fail("Found multiple size controls for " + _tableId);
            Select selectTableLength = new Select(tableLengthControls.get(0));
            selectTableLength.selectByValue(TABLE_LENGTH_MAX);
        }

        // Sort type tables need time to display (not possible without driver).
        // _webElement.waitForElementVisible(dataTable.findElement(By.cssSelector("tbody")));

        WebElement dataTable = _webElement.findElement(By.id(_tableId));
        List<WebElement> dataTableRows = dataTable.findElements(By.xpath(".//tr/td[contains(text(), '"+_rowData+"')]"));

        switch (dataTableRows.size()) {
            case 0:
                System.out.println("No data found for: "+_rowData);
                break;
            case 1:
                return dataTableRows.get(0);
            default :
                System.out.println("Multiple rows found, expecting one for: "+_rowData);
        }
        return null;
    }

    /**
     * Finds and returns the first row for that matches all the data in an array of text string in a given table
     * TODO: expanding tables might need to give this: (in a given webElement)
     * This is useful to find specific rows, but where some will be difficult to match (e.g. Sex, Age)
     * @param _tableId the Id of the table on the page
     * @param _expectedRowData array of data expected to be found in the table
     * @return WebElement for the clickable row of the page for the patient
     *  null if not found
     */
    public WebElement findRowInTable(String _tableId, String[] _expectedRowData) {
        /*
         bug https://bugzilla.mozilla.org/show_bug.cgi?id=1448825 means that using firefox driver you cannot click
         on a table row, so returning the td instead, so that it can be clicked on return.
          */
        String[] currentRowData;

        expandTable(_tableId);

        // Find the rows that contain the first text element
        WebElement _tableElement = driver.findElement(By.id(_tableId));
        List<WebElement> dataTableRows = _tableElement.findElements(By.xpath(".//tr//td[contains(text(), '"+_expectedRowData[0]+"')]"));

        if (dataTableRows.size() > 0) {
            // loop rows
            int rowCount = 0;
            int datumCount = 0;
            boolean rowMatched = false;
            while ((rowCount < dataTableRows.size()) && !rowMatched)
            {
                //Have to find the parent row and then the data
                List<WebElement> rowData = dataTableRows.get(rowCount).findElements(By.xpath("..//td"));
                // Put the Row into an array
                currentRowData = new String[rowData.size()];

                for (WebElement rowDatum:rowData) {
                    currentRowData[datumCount] = rowDatum.getText();
                    datumCount++;
                }

                // loop expected data if match item move to next
                // already matched the first
                int countExpected = 1;
                boolean matchedDatum = true;
                while (countExpected < _expectedRowData.length && matchedDatum) {
                    if (ArrayUtils.contains(currentRowData, _expectedRowData[countExpected])){
                        //found current datum, move to next
                        countExpected++;
                    }
                    else {
                        matchedDatum = false;
                    }
                }
                if (countExpected < _expectedRowData.length) {
                    // didn't find all the items move to next row
                    rowCount++;
                }
                else {
                    rowMatched = true;
                }
            }
            if (rowMatched)
                return dataTableRows.get(rowCount);
        }

        String messageText = "";
        for (String rowDatum:_expectedRowData) {
            messageText = messageText + rowDatum + " ";
        }
        System.out.println("No data found for: " + messageText);
        return null;
    }

    public void checkShowPageContent(String [][] _showData, boolean exactMatch)
    {
        List<WebElement> columnData = driver.findElements(By.className(SHOW_TABLE_CSS));

        int col = 0;
        //iterate through and add +1 to infoRow each time
        for (WebElement column:columnData) {


            String header = column.findElement(By.tagName(SHOW_HEADER_TAG)).getText();
            String data = column.findElement(By.tagName(SHOW_DATA_TAG)).getText();

            Assert.assertEquals("Column heading doesn't match, expected: " + _showData[0][col] + " actual: " + header, _showData[0][col], header );
            Assert.assertEquals("Column data doesn't match, expected: " + _showData[1][col] + " actual: " + data, _showData[1][col], data);
            col++;
        }

    }
}
