package com.baxter.seleniumopat.utilities.driver;


import java.util.List;
import java.util.function.Function;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Decorator for RemoteWebDriver, adds waitFor methods and few other helper
 * methods
 */
public class WaitingRemoteWebDriver extends RemoteWebDriverDecorator {

    /**
     * Base url to help create full urls
     */
    protected String baseUrl;

    /**
     * Default time to wait before next check when using until and waitFor*
     * methods
     */
    public static final long DEFAULT_SLEEP = 500L;

    /**
     * Default time after which until and waitFor methods throw timeout
     * Exception
     */
    public static final long DEFAULT_TIMEOUT = 60L;

    /**
     * Constructor, uses default values for timeout and sleep time
     *
     * @param driver Remote web-driver to be wrapped
     */
    public WaitingRemoteWebDriver(RemoteWebDriver driver) {
        super(driver, DEFAULT_TIMEOUT, DEFAULT_SLEEP);
    }

    /**
     * Constructor, uses default value sleep time
     *
     * @param driver           Remote web-driver to be wrapped
     * @param timeOutInSeconds Timeout value
     */
    public WaitingRemoteWebDriver(RemoteWebDriver driver, long timeOutInSeconds) {
        super(driver, timeOutInSeconds, DEFAULT_SLEEP);
    }

    /**
     * Constructor, uses default value sleep time
     *
     * @param driver           Remote web-driver to be wrapped
     * @param timeOutInSeconds Timeout value
     * @param sleepInMillis    Sleep time between checks in until and waitFor
     *                         methods
     */
    public WaitingRemoteWebDriver(RemoteWebDriver driver, long timeOutInSeconds, long sleepInMillis) {
        super(driver, timeOutInSeconds, sleepInMillis);
    }

    /**
     * @param selector Path to webelement can be xpath or css or selenium
     * @return
     */
    public WebElement click(String selector) {
        WebElement elem = this.findElement(selector);
        elem.click();
        return elem;
    }

    /**
     * uses baseurl to open a page under url
     *
     * @param url
     */
    public void openPage(String url) {

        if (baseUrl.endsWith("/") && url.startsWith("/")) {
            if (url.length() <= 1) {
                driver.get(baseUrl);
                return;
            }
            driver.get(baseUrl + url.substring(1));
        } else {
            driver.get(baseUrl + url);
        }
    }

    /**
     * Waits until it find visible element or it times out
     *
     * @param target Element selector
     * @return
     */
    public WebElement waitForElementVisible(String target) {
        By locator = locator(target);
        WebElement element = until(
            ExpectedConditions.visibilityOfElementLocated(locator)
        );
        return element;
    }

    /**
     * Waits until it find visible element or it times out
     *
     * @param element Element to check
     * @return
     */
    public WebElement waitForElementVisible(WebElement element) {
        return until(
            ExpectedConditions.visibilityOf(element)
        );
    }

    /**
     * Waits until it find visible element or it times out
     *
     * @param parentElem Parent element to check
     * @param target     Selector for child elements
     * @return
     */
    public WebElement waitForElementVisible(WebElement parentElem, String target) {
        List<WebElement> elements = until(
            ExpectedConditions.visibilityOfNestedElementsLocatedBy(parentElem, locator(target))
        );

        return elements.get(0);
    }

    /**
     * Waits until it find element (visible or not) or it times out
     *
     * @param target Element selector
     * @return
     */
    public WebElement waitForElementPresent(String target) {
        return until(
            ExpectedConditions.presenceOfElementLocated(locator(target))
        );
    }

    /**
     * Waits until it finds descendant element (visible or not)
     *
     * @param parentElem Parent element used for searching
     * @param target     Element selector
     * @return
     */
    public WebElement waitForElementPresent(WebElement parentElem, String target) {
        return until(
            ExpectedConditions.presenceOfNestedElementLocatedBy(parentElem, locator(target))
        );
    }

    /**
     * Waits until it not finds element (visible or not) or it times out
     *
     * @param target Element selector
     */
    public void waitForElementNotPresent(String target) {
        until(
            ExpectedConditions.numberOfElementsToBe(locator(target), 0)
        );
    }

    /**
     * Waits until it find element (present or not) or it times out
     *
     * @param target Element selector
     */
    public void waitForElementNotVisible(String target) {
        until(
            ExpectedConditions.invisibilityOfElementLocated(
                locator(target)
            )
        );
    }

    /**
     * Waits until it find element (visible or not) or it times out
     *
     * @param element Element selector
     */
    public void waitForElementNotVisible(WebElement element) {
        until(
            ExpectedConditions.invisibilityOf(element)
        );
    }

    /**
     * Waits until it find element with text present in it
     *
     * @param target Element selector
     */
    public void waitForElementTextPresent(String target, String match) {
        until(
            ExpectedConditions.textToBePresentInElementLocated(
                locator(target), match
            )
        );
    }

    /**
     * Waits until it find element with text not present in it
     *
     * @param target Element selector
     */
    public void waitForElementTextNotPresent(String target, String match) {
        until(
            ExpectedConditions.not(
                ExpectedConditions.textToBePresentInElementLocated(
                    locator(target), match
                )
            )
        );
    }

    /**
     * Waits until it finds alert
     */
    public Alert waitForAlertPresent() {
        until(ExpectedConditions.alertIsPresent());
        return driver.switchTo().alert();
    }

    /**
     * Generic wait method, alias for until
     *
     * @param condition
     */
    public <ElementOrValue extends Object> ElementOrValue waitUntil(Function<? super RemoteWebDriver, ElementOrValue> condition) {
        return until(condition);
    }

    /**
     * Generic wait method, alias for until
     *
     * @param condition
     */
    public <ElementOrValue extends Object> ElementOrValue waitUntil(ExpectedCondition<ElementOrValue> condition) {
        return until(condition);
    }

    /**
     * Finds element in DOM document
     *
     * @param using Element selector, see examples <br />
     *              <ul>
     *              <li>.menu #aboutWebsiteLink</li>
     *              <li>xpath=/html/body/a[title="Link to website"]</li>
     *              <li>/html/body/a[title="Link to website"] </li>
     *              <li>//a[title="Link to website"] </li>
     *              <li>name=genderCheckbox </li>
     *              <li>id=aboutWebsiteLink </li>
     *              <li>class=menuLink </li>
     *              <li>tagName=pre </li>
     *              <li>link=About Website </li>
     *              <li>partialLink=About </li>
     *              </ul
     * @return
     */
    public WebElement findElement(String using) {

        String type = guessSelectorType(using);
        String selectorString = filterSelector(using);

        WebElement elem;

        switch (type) {
            case "xpath":
                elem = driver.findElement(By.xpath(selectorString));
                break;
            case "name":
                elem = driver.findElement(By.name(selectorString));
                break;
            case "link text":
                elem = driver.findElement(By.linkText(selectorString));
                break;
            case "id":
                elem = driver.findElement(By.id(selectorString));
                break;
            case "class name":
                elem = driver.findElement(By.className(selectorString));
                break;
            case "tag name":
                elem = driver.findElement(By.tagName(selectorString));
                break;
            case "partial link name":
                elem = driver.findElement(By.partialLinkText(selectorString));
                break;
            default:
                elem = driver.findElement(By.cssSelector(selectorString));
                break;
        }

        return elem;
    }

    /**
     * Finds element in DOM document
     *
     * @param using Element selector, see examples <br />
     *              <ul>
     *              <li>.menu #aboutWebsiteLink</li>
     *              <li>xpath=/html/body/a[title="Link to website"]</li>
     *              <li>/html/body/a[title="Link to website"] </li>
     *              <li>//a[title="Link to website"] </li>
     *              <li>name=genderCheckbox </li>
     *              <li>id=aboutWebsiteLink </li>
     *              <li>class=menuLink </li>
     *              <li>tagName=pre </li>
     *              <li>link=About Website </li>
     *              <li>partialLink=About </li>
     *              </ul
     * @return
     */
    public By locator(String using) {

        String type = guessSelectorType(using);
        String selectorString = filterSelector(using);

        By loc;

        switch (type) {
            case "xpath":
                loc = By.xpath(selectorString);
                break;
            case "name":
                loc = By.name(selectorString);
                break;
            case "link text":
                loc = By.linkText(selectorString);
                break;
            case "id":
                loc = By.id(selectorString);
                break;
            case "class name":
                loc = By.className(selectorString);
                break;
            case "tag name":
                loc = By.tagName(selectorString);
                break;
            case "partial link name":
                loc = By.partialLinkText(selectorString);
                break;
            default:
                loc = By.cssSelector(selectorString);
                break;
        }

        return loc;
    }

    /**
     * Removes prefix from selector
     *
     * @param using Element selector
     */
    private String filterSelector(String using) {
        String filtered;

        if (using.startsWith("id=")) {
            filtered = using.substring(3);
        } else if (using.startsWith("css=")) {
            filtered = using.substring(4);
        } else if (using.startsWith("name=") || using.startsWith("link=")) {
            filtered = using.substring(5);
        } else if (using.startsWith("xpath=") || using.startsWith("class=")) {
            filtered = using.substring(6);
        } else if (using.startsWith("tagName=")) {
            filtered = using.substring(8);
        } else if (using.startsWith("partialLink=")) {
            filtered = using.substring(12);
        } else {
            filtered = using;
        }

        return filtered;
    }

    /**
     * Tries to guess selector type, if failes then it assumes that is css
     *
     * @param using Selector string
     * @return guessed type of the selector in 'using'
     */
    private String guessSelectorType(String using) {

        if (using.startsWith("xpath=") || using.startsWith("/")) {
            return "xpath";
        }
        if (using.startsWith("name=")) {
            return "name";
        }
        if (using.startsWith("link=")) {
            return "link text";
        }
        if (using.startsWith("id=")) {
            return "id";
        }
        if (using.startsWith("class=")) {
            return "class name";
        }
        if (using.startsWith("tagName=")) {
            return "tag name";
        }
        if (using.startsWith("partialLink=")) {
            return "partial link name";
        }

        return "css selector";
    }

    /**
     * Deletes all visible cookies
     * <p>
     * !Note this method does NOT delete cookies with setting httpOnly=true
     */
    public void deleteAllVisibleCookies() {
        java.util.Iterator<Cookie> it = driver.manage().getCookies().iterator();
        while (it.hasNext()) {
            Cookie cookie = it.next();
            if (!cookie.isHttpOnly()) {
                driver.manage().deleteCookie(cookie);
            }
        }
    }

    /**
     * Deletes visible cookie name as in parameter
     * <p>
     * !Note this method does NOT delete cookies with setting httpOnly=true
     *
     * @param name Name of the cookie
     */
    public void deleteVisibleCookieNamed(String name) {
        java.util.Iterator<Cookie> it = driver.manage().getCookies().iterator();
        while (it.hasNext()) {
            Cookie cookie = it.next();
            if (!cookie.isHttpOnly() && cookie.getName().equals(name)) {
                driver.manage().deleteCookie(cookie);
            }
        }
    }

    /**
     * Moves mouse over an Element with given path
     *
     * @param targetPath path to the
     */
    public void mouseOver(String targetPath) {
        Actions actionBuilder = new Actions(driver);
        actionBuilder.moveToElement(findElement(targetPath))
            .build()
            .perform();
    }

    /**
     * Checks if element exists in DOM structure
     *
     * @param by Criteria used to find element
     * @return
     */
    public boolean isElementPresent(By by) {
        return driver.findElements(by).size() > 1;
    }

    /**
     * Checks if element exists in DOM structure
     *
     * @param selector Criteria used to find element
     * @return
     */
    public boolean isElementPresent(String selector) {
        return driver.findElements(locator(selector)).size() > 1;
    }

    /**
     * Checks if element exists in DOM structure and is visible
     *
     * @param by Criteria used to find element
     * @return
     */
    public boolean isElementVisible(By by) {
        List<WebElement> elements = driver.findElements(by);
        if (elements.size() > 1) {
            return elements.get(0).isDisplayed();
        }
        return false;
    }

    /**
     * Checks if element exists in DOM structure and is visible
     *
     * @param selector Criteria used to find element
     * @return
     */
    public boolean isElementVisible(String selector) {
        List<WebElement> elements = driver.findElements(locator(selector));
        if (elements.size() > 1) {
            return elements.get(0).isDisplayed();
        }
        return false;
    }

    public boolean isAlertPresent() {
        Alert alert = ExpectedConditions.alertIsPresent().apply(driver);
        return (alert != null);

    }

    /**
     * Click which is performed by js
     *
     * @param element
     */
    public WebElement clickViaJavascript(WebElement element) {
        JavascriptExecutor js = driver;
        js.executeScript("arguments[0].click();", element);
        return element;
    }

    /**
     * Click which is performed by js
     *
     * @param using
     */
    public WebElement clickViaJavascript(String using) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement elem = findElement(using);
        js.executeScript("arguments[0].click();", elem);
        return elem;
    }

    /**
     * Getter, base url which is used to create full urls e.g. in openPage
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * Setter, base url is used to create full urls e.g. in openPage
     *
     * @param baseUrl
     */
    public void setBaseUrl(String baseUrl) {
        if (!baseUrl.endsWith("/")) {
            baseUrl = baseUrl + "/";
        }
        this.baseUrl = baseUrl;
    }

}
