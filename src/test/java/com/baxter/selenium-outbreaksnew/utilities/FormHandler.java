package com.baxter.seleniumopat.utilities;

import com.baxter.seleniumopat.utilities.driver.WaitingRemoteWebDriver;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertTrue;

/**
 * NOTE CLASS HAS BEEN MODIFIED FOR OPAT, notably the waitforsave function is not needed
 * in OPAT and has been commented to have no functionality.
 * This would need addressing if merged into a base framework. Also validation of fields was added
 * and there are issue with date fields in bootstrap.
 *
 *
 * The FormHandler class will try to automate certain common tasks that can be
 * used across the application.
 * <p>
 * Example: If a custom testing script containing a list of WebElements of type
 * input text, and values to be tested against these elements, the FormHandler
 * class can be used to automate this process.
 */
public class FormHandler {

    private WaitingRemoteWebDriver driver;

    /**
     * initialise webdriver
     *
     * @param _driver
     */
    public FormHandler(WaitingRemoteWebDriver _driver) {
        driver = _driver;
    }

    /**
     * populate a WebElement with a given value
     *
     * @param selector String selector value to be searched for
     * @return WebElement
     */
    public WebElement populate(String selector, String value) {
        if (driver.isAlertPresent()) {
            driver.switchTo().alert().accept();
        }
        WebElement element = driver.findElement(selector);
        return populate(element, value);
    }

    /**
     * populate a WebElement with a given values
     *
     * @param selector String selector value to be searched for
     * @param values   Array of values to populate
     * @return WebElement
     */
    public WebElement populate(String selector, String[] values) throws NotImplementedException {

        WebElement element = driver.findElement(selector);

        if (isMultipleChoiceElement(element)) {
            element = populate(element, values);
        } else if (values.length > 0) {
            element = populate(element, values[0]);
        }

        return element;
    }

    /**
     * populate a WebElement with a given value
     *
     * @param element Element to be populated
     * @param value   value to populate
     * @return WebElement
     */
    public WebElement populate(WebElement element, String value) {

        String type = element.getAttribute("type");
        boolean waitForSave = false; // turn this on for tick box confirmations (PPS)

        switch (element.getTagName()) {
            case "input":
                if (type.equals("radio")) {
                    WebElement form = element;

                    while (!form.getTagName().equals("form")) {
                        form = form.findElement(By.xpath("./.."));
                    }
                    element = form.findElement(
                            By.cssSelector(
                                String.format(
                                    "input[name='%s'][value='%s']",
                                    element.getAttribute("name"),
                                    value
                                )
                            )
                    );
                    if (element.isSelected()) {
                        waitForSave = false;
                    } else {
                        driver.executeScript("arguments[0].click();", element);
                    }
                } else if (type.equals("checkbox")) {
                    clear(element);
                    element = driver.findElement(
                        String.format(
                            "input[name='%s'][value='%s']",
                            element.getAttribute("name"),
                            value
                        )
                    );
                    element.click();
                } else {
                    clear(element);
                    String prevValue = element.getAttribute("value");
                    if (prevValue.equals(value)) {
                        waitForSave = false;
                    } else {
                        element.sendKeys(value);// write new value
                    }
                }
                break;
            case "textarea":
                clear(element);
                String prevValue = element.getText();
                if (prevValue.equals(value)) {
                    waitForSave = false;
                } else {
                    element.sendKeys(value);// write new value
                }
                break;
            case "select":
                select(element, value);
                waitForSave = false;
                break;
            default:
                waitForSave = false;
                break;
        }
        if (waitForSave) {
            waitForSave(element);
        }

        if (element.isDisplayed() && !element.getTagName().equals("select")) {
            element.sendKeys(Keys.TAB);
        }

        return element;
    }

    /**
     * populate a WebElement with a given value
     *
     * @param multiChoiceElem
     * @param values          Array of values to populate
     * @return
     * @throws NotImplementedException
     */
    public WebElement populate(WebElement multiChoiceElem, String[] values) throws NotImplementedException {
        if (!isMultipleChoiceElement(multiChoiceElem)) {
            if (values.length > 0) {
                populate(multiChoiceElem, values[0]);
            }
            return multiChoiceElem;
        }

        //todo
        throw new NotImplementedException();
    }

    /**
     * populate list of fields
     *
     * @param fieldList Map of field selectors and values
     * @throws NotImplementedException
     */
    public void populate(HashMap<String, Object> fieldList) throws NotImplementedException {
        // @todo finish
        throw new NotImplementedException();

    }

    /**
     * Ticks checkbox or radio button
     *
     * @param selector Selector string
     * @return
     */
    public WebElement tick(String selector) {

        WebElement element = driver.findElement(selector);

        populate(element, element.getAttribute("value"));

        return element;
    }

    /**
     * Checks if element is multiple choice
     *
     * @param elem
     * @return
     */
    private boolean isMultipleChoiceElement(WebElement elem) {

        if (elem.getTagName().equals("select") && elem.getAttribute("multiple") != null) {
            return true;
        }

        return elem.getAttribute("name").endsWith("[]");
    }

    /**
     * @param selector
     * @return
     */
    public WebElement clear(String selector) {
        return clear(driver.waitForElementVisible(selector));
    }

    public WebElement clear(WebElement element) {
        // TODO if this is to be part of the framework the waitForSave needs to be parameterised

        // TODO: Lewis edit: check this, if the element is a checkbox and its not checked, it
        // carried on to the input and failed
        // if ("checkbox".equals(element.getAttribute("type")) && element.isSelected()) {
        if ("checkbox".equals(element.getAttribute("type"))) {
            if (element.isSelected())
                element.click();
//            waitForSave(element);
        } else if ("input".equals(element.getTagName())) {
            if (!"".equals(element.getAttribute("value"))) {
                element.clear();
//                waitForSave(element);
            }
        } else if (element.getTagName().equals("textarea")) {
            if (!"".equals(element.getText())) {
                element.clear();
//                waitForSave(element);
            }
        } else if (element.getTagName().equals("select")) {
            Select select = new Select(element);
            if (select.isMultiple() && select.getAllSelectedOptions().size() > 0) {
                select.deselectAll();
//                waitForSave(element);
            }
        }

        return element;
    }

    private WebElement waitForSave(WebElement formElement) {
        //TODO: this is getting called when we don't need it in OPAT
        //so needs figuring out for framework.
        return driver.waitForElementVisible(formElement);

/**
        if (driver.isAlertPresent()) {
            driver.switchTo().alert().accept();
        }
        String idPart = formElement.getAttribute("name");
        String type = formElement.getAttribute("type");
        if (formElement.getTagName().equals("input") && (type.equals("radio") || type.equals("checkbox"))) {
            idPart += "_" + formElement.getAttribute("value");
        }
        idPart += "_saved";

        **
         * wait for the saved "tick" to appear
         *
        String tickXpath = "//span[contains(@id, '" + idPart + "')]";
        return driver.waitForElementVisible(tickXpath);
 **/
    }

    public WebElement select(WebElement element, String value) {

        if (element.getAttribute("class").contains("select2-hidden-accessible")) {
            // this is select2
            return select2(element, value);
        }

        /**
         * change value in order to trigger saving
         */
        Select select = new Select(element);

        if (select.getAllSelectedOptions().size() > 0) {
            select.selectByIndex(select.getOptions().size() - 1);
            waitForSave(element);
        }

        /**
         * enter new value
         */
        Select newSelect = new Select(element);
        if (value.equals("")) {
            newSelect.selectByIndex(0);
        } else {
            newSelect.selectByValue(value);
        }

        waitForSave(element);

        return element;
    }

    /**
     * Selects value on select2 element
     *
     * @param element select tag element
     * @param value   value to be selected (this is NOT label)
     * @return
     */
    private WebElement select2orig(WebElement element, String value) {
        String id = element.getAttribute("id");
        WebElement container = driver.findElement(
            "#select2-" + element.getAttribute("id") + "-container"
        );

        String label = element.findElement(By.cssSelector(String.format("option[value='%s']", value)))
            .getText();

        container.click();
        driver.waitForElementVisible("input.select2-search__field").sendKeys(label);
        /**
         * This type of web element doesn't handle WebElement::click() very well if part of the
         * element is out of the screen.
         * Using:
         *     driver.executeScript("arguments[0].scrollIntoView();", webElement);
         *
         *     also doesn't seem to scroll to element, or positions the element with part of it still
         *     outside the screen.
         * Because of the above, some tests are triggering exception "element is not clickable at point..."
         *
         * Using the script below, will scroll to element + its size, meaning, all of it will be visible
         * in the screen.
         */
        WebElement webElement = driver.waitForElementVisible(
                String.format("//ul[@id='select2-%s-results']/li[text()='%s']", id, label)
        );
        driver.executeScript("window.scrollTo(0,"+webElement.getLocation().y+")");
        webElement.click();
        return element;
    }


    /**
     * Selects value on select2 element
     * Allows adding a new one if the input doesn't exist
     * select2orig is the original code
     * Essentially, just take out the check for the existence of the
     * value and add it if it isn't there;
     * DOING THIS MAY CAUSE SOME EXISTING PPS TESTS TO PASS THAT WERE
     * PREVIOUSLY EXPECTED TO FAIL
     *
     * @param element select tag element
     * @param value   value to be selected (this is NOT label)
     * @return
     */
    private WebElement select2(WebElement element, String value) {
        //TODO: not tested when multiple select2 are on a page
        String id = element.getAttribute("id");

        //find the container
        WebElement container = driver.findElement("#select2-" + id + "-container");

        // find the following sibling toggle-arrow on the element container
        // and click to expand the selection
        container.findElement(By.xpath("../span[@class='select2-selection__arrow']")).click();

        // TODO: only one of these should be displayed
        // todo if there are multiples then need to check the right one
        // selection should now be visible for the options
        // this is not in the container rendered at foot of HTML
        driver.waitForElementVisible("input.select2-search__field").sendKeys(value);

        /*
         * This type of web element doesn't handle WebElement::click() very well if part of the
         * element is out of the screen.
         * Using:
         *     driver.executeScript("arguments[0].scrollIntoView();", webElement);
         *
         *     also doesn't seem to scroll to element, or positions the element with part of it still
         *     outside the screen.
         * Because of the above, some tests are triggering exception "element is not clickable at point..."
         *
         * Using the script below, will scroll to element + its size, meaning, all of it will be visible
         * in the screen.
         */

         WebElement webElement = driver.waitForElementVisible(
                        String.format("//ul[@id='select2-%s-results']/li[text()='%s']", id, value)
                );
         driver.executeScript("window.scrollTo(0,"+webElement.getLocation().y+")");
         webElement.click();
         return element;
    }

    /**
     * Load elements from testing script file and populate them with given
     * values TODO using html element name property only; make it generic!
     *
     * @param _testScriptFile Test Script file name
     * @return List<WebElement> webElements
     */
    public List<WebElement> populateFromTestScript(String _testScriptFile) {
        ObjectMapper mapper = new ObjectMapper();

        mapper.addHandler(new DeserializationProblemHandler() {
            @Override
            public boolean handleUnknownProperty(DeserializationContext ctxt, JsonParser p, JsonDeserializer<?> deserializer, Object beanOrClass, String propertyName) throws IOException {
                return true;
            }
        });

        List<WebElement> webElements = new ArrayList<WebElement>();

        //JSON from file to Object
        try {

            // load test script file
            ClassLoader classLoader = FormHandler.class.getClassLoader();
            File scriptFile = new File(classLoader.getResource("scripts/" + _testScriptFile).getFile());

            // map elements into a list of MyElements
            List<ScriptElement> scriptElements = Arrays.asList(mapper.readValue(scriptFile, ScriptElement[].class));

            String selector;
            WebElement webElement;

            for (ScriptElement scriptElem : scriptElements) {
                System.out.println("Filling " + scriptElem.name + ":" + scriptElem.value);

                selector = "name=" + scriptElem.name;
                // iterate through the list and populate accordingly
                if (driver.isAlertPresent()) {
                    driver.switchTo().alert().accept();
                }
                webElement = populate(selector, scriptElem.value.trim());
                webElements.add(webElement);
            }
        } catch (java.io.IOException e) {

            e.printStackTrace();
        }

        return webElements;
    }

    /**
     * ScriptElement is a translation of the JSON objects found in the test
     * script file
     */
    private static class ScriptElement {

        /**
         * Name attribute of the html element
         */
        public String name;

        /**
         * Value attribute of the html element
         */
        public String value;

        /**
         * Default constructor
         */
        public ScriptElement() {
        }
    }

    /****************************************************
     * Form validation methods, taken from above
     * but validate current values against file
     ****************************************************/

    /**
     * validate a WebElement has a given value
     *
     * @param element Element to be validated
     * @param value   value to validated
     * @return WebElement
     */
    public WebElement validate(WebElement element, String value) {

        String type = element.getAttribute("type");
        String elementName = element.getAttribute("name");

        switch (element.getTagName()) {
            case "input":
                if (type.equals("radio")) {
                    WebElement form = element;

                    while (!form.getTagName().equals("form")) {
                        form = form.findElement(By.xpath("./.."));
                    }
                    element = form.findElement(
                            By.cssSelector(
                                    String.format(
                                            "input[name='%s'][value='%s']",
                                            element.getAttribute("name"),
                                            value
                                    )
                            )
                    );
                    assertTrue("Radio button "+elementName+ " not selected", element.isSelected());
                } else if (type.equals("checkbox")) {
                    element = driver.findElement(
                            String.format(
                                    "input[name='%s'][value='%s']",
                                    element.getAttribute("name"),
                                    value
                            )
                    );
                    //TODO: not tested
                    assertTrue("Check box "+elementName+ " not selected", element.isSelected());
                } else {
                    String prevValue = element.getAttribute("value");
                    assertTrue("Field mismatch: "+elementName+ ", Expected: "+value+" found: "+prevValue,
                            prevValue.equals(value));
                }
                break;
            case "textarea":
                //TODO: not tested
                String prevValue = element.getText();
                assertTrue("Field mismatch: "+elementName+ ", Expected: "+value+" found: "+prevValue,
                        prevValue.equals(value));
                break;
            case "select":
                Select select = new Select(element);
                String selectedText = select.getFirstSelectedOption().getText();
                assertTrue(elementName + " " + value + " not selected ("+selectedText+")", value.equals(selectedText));
                break;
            default:
                break;
        }

        if (element.isDisplayed() && !element.getTagName().equals("select")) {
            element.sendKeys(Keys.TAB);
        }

        return element;
    }

    /**
     * validate a WebElement with a given value
     *
     * @param selector String selector value to be searched for
     * @return WebElement
     */
    public WebElement validate(String selector, String value) {
        WebElement element = driver.findElement(selector);
        return validate(element, value);
    }

    /**
     * Load elements from testing script file and checks the content
     * values on the form for a match
     * TODO using html element name property only; make it generic!
     *
     * @param _testScriptFile Test Script file name
     * @return List<WebElement> webElements
     */
    public List<WebElement> validateFromTestScript(String _testScriptFile) {
        ObjectMapper mapper = new ObjectMapper();

        mapper.addHandler(new DeserializationProblemHandler() {
            @Override
            public boolean handleUnknownProperty(DeserializationContext ctxt, JsonParser p, JsonDeserializer<?> deserializer, Object beanOrClass, String propertyName) throws IOException {
                return true;
            }
        });

        List<WebElement> webElements = new ArrayList<WebElement>();

        //JSON from file to Object
        try {

            // load test script file
            ClassLoader classLoader = FormHandler.class.getClassLoader();
            File scriptFile = new File(classLoader.getResource("scripts/" + _testScriptFile).getFile());

            // map elements into a list of MyElements
            List<ScriptElement> scriptElements = Arrays.asList(mapper.readValue(scriptFile, ScriptElement[].class));

            String selector;
            WebElement webElement;

            for (ScriptElement scriptElem : scriptElements) {
                System.out.println("Validating " + scriptElem.name + ":" + scriptElem.value);

                selector = "name=" + scriptElem.name;
                // iterate through the list and validate accordingly
                webElement = validate(selector, scriptElem.value);
                webElements.add(webElement);
            }
        } catch (java.io.IOException e) {

            e.printStackTrace();
        }

        return webElements;
    }
}
