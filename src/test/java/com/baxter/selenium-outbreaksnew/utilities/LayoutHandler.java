package com.baxter.seleniumopat.utilities;

import com.baxter.seleniumopat.pages.PatientPanel;
import com.baxter.seleniumopat.utilities.driver.WaitingRemoteWebDriver;
import org.apache.commons.lang.ArrayUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * The layout handler for OPAT will check that the screen is displaying the correct layout
 * in terms of the header banner has the correct links, the LH menu has the correct elements
 * and if the Patient profile panel is displayed where needed
 */

/**
 * TODO: refactor layout handling
 * NOTE: with hindsight it might have been better to have an object for the action menu and
 * another for the banner stripe and put the checks for those in the object; similar to the patient panel
 * The layout handler can then just check which panels should be visible when loading the overall screens
 */
public class LayoutHandler {
    public enum PageType {ADD, EDIT, SHOW, WIZARD}

    public static final String KEYCLOAK_LOGIN_FORM_ID = "kc-form-login";
    public static final String BANNER_CLASS_NAME = "topbar";
    public static final String BANNER_PROFILE_ICON_CLASS = "nav-user";
    public static final String BANNER_PROFILE_LOGOUT_CSS = ".profile-dropdown .mdi-logout";
    private static final String BANNER_TITLE = "OPAT";

    private static final String MENU_HEADING_CLASS = "menu-title";
    private static final String MENU_BAR = "sidebar-menu";
    private static final String ACTION_BUTTON_CLASS = "actions";
    private static final String ALERT_ICON_CLASS_NAME = "mdi-bell";
    private static final String ALERT_COUNT_CLASS_NAME = "noti-icon-badge";
    private static final String ALERT_PANEL_CSS = "div.dropdown-menu.show";
    private static final String ALERT_XPATH = "//p[contains(text(),'%s episode has ended')]";

    private static final String MAP_ID = "gmap";
    private static final String MAP_PANEL_ID = "summary-address-map";

    private WaitingRemoteWebDriver driver;

    /**
     * initialise webdriver
     *
     * @param _driver page driver
     */
    public LayoutHandler(WaitingRemoteWebDriver _driver) {
        driver = _driver;
    }

    /**
     * Check that a given list of elements exist on the menu
     * @param _menuItemIds list of elements expected on the menu by id
     *
     */
    private void checkMenuItems(String[] _menuItemIds) {
        WebElement menuBar = driver.findElementById(MENU_BAR);

        // make sure the elements are present
        for (String menuItemId : _menuItemIds) {
            menuBar.findElement(By.id(menuItemId));
            System.out.println("Menu Item found: " + menuItemId);
        }

        // Check the number of valid menu selections
        assertEquals("Number of menu elements incorrect: ", _menuItemIds.length,
                menuBar.findElements(By.tagName("li")).size() - menuBar.findElements(By.className(MENU_HEADING_CLASS)).size());

    }

    /**
     * Checks that the buttons at the bottom of a form are correct
     * @param _formId form to check
     * @param _actionButtonIds list of button ids to check
     */
    public void checkActionItems(String _formId, String[] _actionButtonIds) {
        String buttonId;
        int numberFound = 0;

        System.out.println("Checking Action buttons:");

        WebElement actionBar = driver.findElementById(_formId).findElement(By.className(ACTION_BUTTON_CLASS));
        // on some pages the button elements can be different tag types
        List<WebElement> actionBarListItems = actionBar.findElements(By.xpath("*"));

        // if list style then get the list items
        if (actionBarListItems.get(0).getTagName().equals("ul")) {
            actionBarListItems = actionBar.findElements(By.tagName("a"));
        }

        for (WebElement actionBarListItem : actionBarListItems) {
            if (actionBarListItem.isDisplayed()) {
                buttonId = actionBarListItem.getAttribute("id");
                assertTrue("Unexpected button found: "+buttonId+":"+actionBarListItem.getText(), ArrayUtils.contains(_actionButtonIds, buttonId));
                numberFound++;
            }
        }

        // Check the number of valid menu selections
        assertEquals("Number of Action buttons incorrect: ", _actionButtonIds.length, numberFound);
    }


//TODO: should be using driver.waitForElementNotVisible();??
//waitForElementNotVisible needs driver
    /**
     * Check for an element on the page
     * @param _className class name of the element to be checked
     * @return  true if there is at least one visible element on the page
     *          false if there is no visible element or if it does not exist
     */
    public boolean isClassPresent(String _className) {
        List<WebElement>foundElements = driver.findElementsByClassName(_className);

        for (WebElement foundElement : foundElements) {
            if (foundElement.isDisplayed()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check for an element on the page
     * @param _id class name of the element to be checked
     * @return  true if there is one visible element or more than one on the form
     *          false if there is no visible element or if it does not exist
     */
    public boolean isIdPresent(String _id) {
        List<WebElement>foundElements = driver.findElementsById(_id);

        for (WebElement foundElement : foundElements) {
            if (foundElement.isDisplayed()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check for an element on the page
     * @param _id class name of the element to be checked
     * @return  true if there is one visible element or more than one on the form
     *          false if there is no visible element or if it does not exist
     */
    public static boolean isIdPresent(WebElement location, String _id) {
        List<WebElement>foundElements = location.findElements(By.id(_id));

        for (WebElement foundElement : foundElements) {
            if (foundElement.isDisplayed()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check that the OPAT banner exists and has the menu, collapse
     * Help and User profile
     * TODO: search box
     */
    private void checkBannerItems(boolean _checkBannerVisible) {
        WebElement bannerPanel;
        if (_checkBannerVisible) {
            bannerPanel = driver.findElementByClassName(BANNER_CLASS_NAME);

// Title currently in the logo
//           assertEquals("Title Mismatch", BANNER_TITLE, bannerPanel.findElement(By.xpath(".//span")).getText());
            // logo
            bannerPanel.findElement(By.className("logo")).isDisplayed();
            // menu burger
            bannerPanel.findElement(By.className("button-menu-mobile")).isDisplayed();
            // Help button
            bannerPanel.findElement(By.className("mdi-help-circle")).isDisplayed();
            // User profile button
            bannerPanel.findElement(By.className("mdi-account-circle")).isDisplayed();

            //TODO following will need URLs updating for correct pages
            bannerPanel.findElement(By.cssSelector(".arrow-none")).getAttribute("href").contains("opat.local:8000/home#");
            bannerPanel.findElement(By.cssSelector(".nav-user")).getAttribute("href").contains("opat.local:8000/home#");
        }
        else {
            // ensure panel not there
            assertFalse("Found Banner Panel when not expected", isClassPresent(BANNER_CLASS_NAME));
        }
    }

    /**
     * Check that the OPAT banner has the alert icon and
     * the number is correct; check the number in the list
     * corresponds to the indicated number;
     * if _numberOfAlerts = 0 then no alert icon should be present
     * @param _numberOfAlerts number that should be on the icon and in the list
     */
    public void checkAlerts(String _numberOfAlerts) {
        System.out.println("* Checking alerts *");
        if (_numberOfAlerts.equals("0")) {
            // ensure panel not there
            assertFalse("Found Alert icon when not expected", isClassPresent(ALERT_ICON_CLASS_NAME));
        }
        else {
            WebElement bannerPanel = driver.findElementByClassName(BANNER_CLASS_NAME);
            assertTrue("Alert not found when expected", isClassPresent(ALERT_ICON_CLASS_NAME));
            WebElement alertButton = bannerPanel.findElement(By.className(ALERT_COUNT_CLASS_NAME));
            assertEquals("Alert count mismatch on icon", _numberOfAlerts, alertButton.getText());
            // TODO could check dropdown not present until clicked if it had an id
            // the class is used elsewhere

            //click Alerts icon, check number and list in dropdown
            alertButton.click();
            WebElement alertListPanel = bannerPanel.findElement(By.cssSelector(ALERT_PANEL_CSS));
            assertEquals("Alert count mismatch on list",_numberOfAlerts,alertListPanel.findElement(By.cssSelector("span.badge")).getText());
            List<WebElement> alertList = alertListPanel.findElements(By.tagName("a"));
            assertEquals("Alert items mismatch on list",_numberOfAlerts,Integer.toString(alertList.size()));
            alertButton.click(); // dismiss alerts
        }
    }

    /**
     * Open the Alerts and click on a given element
     * @param _patientName name of patient in the alert
     */
    public void clickAlert(String _patientName) {
        System.out.println("* Clicking alert "+_patientName+" *");
        WebElement bannerPanel = driver.findElementByClassName(BANNER_CLASS_NAME);
        WebElement alertButton = bannerPanel.findElement(By.className(ALERT_COUNT_CLASS_NAME));
        alertButton.click();
        WebElement alertListPanel = bannerPanel.findElement(By.cssSelector(ALERT_PANEL_CSS));

        // should only ever be one per patient
        alertListPanel.findElement(By.xpath(String.format(ALERT_XPATH,_patientName))).click();
    }

    /**
     * Check the Patient Profile panel
     * @param _checkProfileVisible if true, check the profile is visible and correct
     *                             if false, ensure not present or not visible
     */
    private void checkPatientProfile(boolean _checkProfileVisible) {
        WebElement profilePanel;

        if (_checkProfileVisible) {
            //find and check profile
            System.out.println("Checking Profile is shown");
            assertTrue("Profile Panel not found", isIdPresent(PatientPanel.PROFILE_PANEL));
            assertTrue("Google map missing from patient summary", isIdPresent(MAP_ID));
        }
        else {
            // ensure panel not there
            System.out.println("Checking Profile is NOT shown");
            assertFalse("Unexpected Profile Panel found", isIdPresent(PatientPanel.PROFILE_PANEL));
            assertFalse("Unexpected Google map found", isIdPresent(MAP_PANEL_ID));
        }
    }

    /**
     * Check that the page layout fits the convention and has correct items
     * @param _pageTitle expected title of the page
     * @param _menuItemIds list of menu items by Id
     * @param _checkBannerVisible is Banner visible and correct?
     * @param _checkProfileVisible is Patient Profile visible and correct
     */
    public void checkPageLayout(String _pageTitle, String[] _menuItemIds, boolean _checkBannerVisible, boolean _checkProfileVisible) {

        assertEquals("Title Mismatch", _pageTitle, driver.getTitle());
        checkMenuItems(_menuItemIds);
        checkBannerItems(_checkBannerVisible);
        checkPatientProfile(_checkProfileVisible);
   }
}
