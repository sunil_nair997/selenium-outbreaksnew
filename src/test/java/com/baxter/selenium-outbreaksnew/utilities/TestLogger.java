package com.baxter.seleniumopat.utilities;

import java.io.IOException;
import java.util.logging.*;

public class TestLogger {

    Logger logger;
    String className;
    Config config = new Config();

    public TestLogger(String _className) {

        System.setProperty("java.util.logging.config.file",
                this.getClass().getClassLoader().getResource("logger.properties").getFile());

        className = _className;
        logger = Logger.getLogger(className);

        initialiseLogger();
    }

    private void initialiseLogger() {

        logger.setLevel(Level.FINE);

        try {

            StringBuilder fileName = new StringBuilder();
            fileName.append(config.getProperty("logsdir"));
            fileName.append("/");
            fileName.append(className);

            Handler fileHandler = new FileHandler(fileName.toString());
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);

        } catch (SecurityException | IOException e) {

            System.out.println("Log file not found. Using standard error output stream.");
            System.out.println("Please refer to resources/config.properties and assign a directory where to save the logs.");
        }
    }

    public Logger get() {

        return logger;
    }
}
