package com.baxter.seleniumopat.utilities;

import java.util.Properties;

/**
 * This class handles configuration properties that are required in the
 * test environment.
 *
 * It loads the file config.properties from resources and hands over properties through
 * the getProperty method.
 */
public class Config {

    Properties configFile;

    public Config() {

        configFile = new java.util.Properties();

        // configuration file is stored in the test resources folder
        try {

            configFile.load(this.getClass().getResourceAsStream("/config.properties"));
        }
        catch(Exception e){

            e.printStackTrace();
        }
    }

    /**
     * Returns the value for the property requested
     *
     * @param property
     * @return String property value
     */
    public String getProperty(String property) {

        String propertyValue = this.configFile.getProperty(property);

        // check if property exists before returning, to avoid nullpointerexception when returning
        if (propertyValue == null)
            return "";

        return this.configFile.getProperty(property);
    }
}
