package com.baxter.seleniumopat.utilities;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * The data in here relates to the SeleniumSeeder in the OPAT project
 * It is used after seeding that data to check the application.
 *
 * To seed this data navigate to the host Homestead folder and:
 *
 * vagrant ssh
 * cd code/opat
 * php artisan migrate:refresh
 * php artisan db:seed --class=SeleniumSeeder
 */

public class PatientData {
    public static final String PANEL_LABEL_NAME = "Name";
    public static final String PANEL_LABEL_PRIMARY_ID = "Primary Identifier";
    public static final String PANEL_LABEL_SECONDARY_ID = "Secondary ID";
    public static final String PANEL_LABEL_TERTIARY_ID = "Tertiary ID";
    public static final String PANEL_LABEL_DOB = "Date of Birth";
    public static final String PANEL_LABEL_SEX = "Sex";
    public static final String PANEL_LABEL_PRIMARY_PHONE = "Primary phone";

    public static final String PANEL_LABEL_SECONDARY_PHONE = "Secondary phone";
    public static final String PANEL_LABEL_EMAIL = "Email";
    public static final String PANEL_LABEL_CARER_NAME = "Carer";
    public static final String PANEL_LABEL_CARER_PHONE = "Carer phone";
    public static final String PANEL_LABEL_CARER_RELATIONSHIP = "Relationship";

    public static final String SET_DOTS = "<Set DOTs>";
    public static final String SET_TODAY = "<Set Today>";
    public static final String SET_DATES = "%s - %s";

    public static final String TIMELINE_EPISODE_ENDED = "Episode ended";
    public static final String TIMELINE_EPISODE_STARTED = "Episode started";
    public static final String TIMELINE_MEDICATION_ENDED = "Medication ended";
    public static final String TIMELINE_MEDICATION_STARTED = "Medication started";
    public static final String TIMELINE_NOTE_CREATED = "Created: %s , %s";
    public static final String TIMELINE_NOTE_EPISODE = "Episode note";
    public static final String TIMELINE_NOTE_PATIENT = "Patient note";
    public static final String TIMELINE_NOTE_UPDATED = "Last edit: %s , %s";
    public static final String TIMELINE_REACTION = "Adverse reaction";
    public static final String TIMELINE_CHECK = "Suitability check";
    public static final int TIMELINE_DATE_EDIT_INDEX = 3;
    public static final int TIMELINE_DATE_SET_INDEX = 2;

    /* If data manipulation of the maps is needed in the tests then it needs a method
    to return a new map otherwise the actual object here is manipulated for all tests in the suite:

    public static final Map<String, String> getMap() {
        return new LinkedHashMap<String,String>()
        {{
            put(PANEL_LABEL_NAME, ADDREACTION_NAME);
            put(PANEL_LABEL_PRIMARY_ID, "addreaction-1");
            put(PANEL_LABEL_TERTIARY_ID, "addreaction-3");
            put(PANEL_LABEL_DOB, "05-May-2005");
            put(PANEL_LABEL_SEX, "Female");
            put(PANEL_LABEL_PRIMARY_PHONE, "0376 035 1557");
        }};
    }

    */

    public static final String ADDREACTION_NAME = "ADDREACTION-FAMILY, Addreaction-given";
    public static final Map<String, String> ADDREACTION_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, ADDREACTION_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "addreaction-1");
        put(PANEL_LABEL_TERTIARY_ID, "addreaction-3");
        put(PANEL_LABEL_DOB, "05-May-2005");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "0376 035 1557");
    }};
    public static final String[] ADDREACTION_ADD_ADVERSE_REACTION = {"Diarrhoea (antibiotic induced)","Drug","08-Aug-2018"};

    public static final String CANDIDATE_NAME = "CANDIDATE-FAMILY, Candidate-given";
    public static final Map<String, String> CANDIDATE_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, CANDIDATE_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "candidate-1");
        put(PANEL_LABEL_SECONDARY_ID, "candidate-2");
        put(PANEL_LABEL_TERTIARY_ID, "candidate-3");
        put(PANEL_LABEL_DOB, "01-Jan-2001");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234000000");

    }};

    public static final String NOTE_NAME = "NOTE-FAMILY, Note-given";
    public static final Map<String, String> NOTE_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, NOTE_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "note-1");
        put(PANEL_LABEL_SECONDARY_ID, "note-2");
        put(PANEL_LABEL_TERTIARY_ID, "note-3");
        put(PANEL_LABEL_DOB, "01-Jan-2001");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234000000");

    }};
    public static final String NOTE_EPISODE_NOTE_ADD = "This is an Episode note!";
    public static final String[][] NOTE_EPISODE_NOTE_ADD_SUMMARY = {
            {
                    TIMELINE_NOTE_EPISODE,
                    NOTE_EPISODE_NOTE_ADD,
                    SET_TODAY,
                    ""
            }};

    public static final String NOTE_PATIENT_NOTE_ADD = "This is a Patient note!";
    public static final String[][] NOTE_PATIENT_NOTE_ADD_SUMMARY = {
            {
                    TIMELINE_NOTE_PATIENT,
                    NOTE_PATIENT_NOTE_ADD,
                    SET_TODAY,
                    ""
            }};

    public static final String[] NOTE_EPISODE_FIRST_NOTE_SUMMARY = {
                    TIMELINE_NOTE_EPISODE,
                    "Discard changes",
                    "Created: 19-Oct-2018 , Neil Armstrong",
                    ""
            };
    public static final String[] NOTE_PATIENT_FIRST_NOTE_SUMMARY = {
                    TIMELINE_NOTE_PATIENT,
                    "One small note",
                    "Created: 19-Oct-2018 , Neil Armstrong",
                    ""
            };
    public static final String NOTE_PATIENT_NOTE_EDIT = "Change Patient note to Episode note!";
    public static final String[][] NOTE_PATIENT_NOTE_EDIT_SUMMARY = {
            {
                    TIMELINE_NOTE_PATIENT,
                    NOTE_PATIENT_NOTE_EDIT,
                    "Created: 19-Oct-2018 , Neil Armstrong",
                    SET_TODAY
            }};

    public static final String CANDADD_NAME = "CANDADD-FAMILY, Candadd-given";
    public static final Map<String, String> CANDADD_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, CANDADD_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "candadd-1");
        put(PANEL_LABEL_SECONDARY_ID, "candadd-2");
        put(PANEL_LABEL_TERTIARY_ID, "candadd-3");
        put(PANEL_LABEL_DOB, "01-Jan-2001");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234010101");
    }};
    public static final String[][] CANDADD_ADD_EPISODE = {{"09-Aug-2018 - N/A",SET_DOTS,"Cerebral abscess"}};

    public static final String CANDEDIT_NAME = "CANDEDIT-FAMILY, Candedit-given";
    public static final Map<String, String> CANDEDIT_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, CANDEDIT_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "candedit-1");
        put(PANEL_LABEL_SECONDARY_ID, "candedit-2");
        put(PANEL_LABEL_TERTIARY_ID, "candedit-3");
        put(PANEL_LABEL_DOB, "10-Oct-1970");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234020202");
    }};

    public static final String CANDADDCHECK_NAME = "CANDADDCHECK-FAMILY, Candaddcheck-given";
    public static final Map<String, String> CANDADDCHECK_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, CANDADDCHECK_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "candaddcheck-1");
        put(PANEL_LABEL_SECONDARY_ID, "candaddcheck-2");
        put(PANEL_LABEL_TERTIARY_ID, "candaddcheck-3");
        put(PANEL_LABEL_DOB, "10-Oct-1970");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234000001");
    }};

    public static final String CURRCANCEL_NAME = "CURRCANCEL-FAMILY, Currcancel-given";
    public static final Map<String, String> CURRCANCEL_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, CURRCANCEL_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "currcancel-1");
        put(PANEL_LABEL_SECONDARY_ID, "currcancel-2");
        put(PANEL_LABEL_TERTIARY_ID, "currcancel-3");
        put(PANEL_LABEL_DOB, "12-Dec-2012");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "012342222222");
    }};
    public static final String[][] CURRCANCEL_EPISODE = {{"01-Aug-2018 - N/A",SET_DOTS,"Congenital CMV"}};

    public static final String CURRADDMED_NAME = "CURRADDMED-FAMILY, Curraddmed-given";
    public static final Map<String, String> CURRADDMED_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, CURRADDMED_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "curraddmed-1");
        put(PANEL_LABEL_SECONDARY_ID, "curraddmed-2");
        put(PANEL_LABEL_TERTIARY_ID, "curraddmed-3");
        put(PANEL_LABEL_DOB, "11-Nov-2011");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234333333");
    }};
    public static final String[][] CURRADDMED_MEDICATIONS_SUMMARY = {{
            "Flucloxacillin",
            "<Set Date>"}};
    public static final String[][] CURRADDMED_EPISODE = {{"02-Jun-2018 - N/A",SET_DOTS,"Discitis/vertebral osteomyelitis - metalwork"}};

    public static final String CURREDITEND_NAME = "CURREDITEND-FAMILY, Curreditend-given";
    public static final Map<String, String> CURREDITEND_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, CURREDITEND_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "curreditend-1");
        put(PANEL_LABEL_SECONDARY_ID, "curreditend-2");
        put(PANEL_LABEL_TERTIARY_ID, "curreditend-3");
        put(PANEL_LABEL_DOB, "11-Nov-2011");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234444444");
    }};
    public static final String[][] CURREDITEND_EPISODE = {{"02-Aug-2018 - N/A",SET_DOTS,"Discitis/vertebral osteomyelitis - metalwork"}};
    public static final int CURREDITEND_EPISODE_HOURS = 16;

    public static final String CURREDITENDFUTURE_NAME = "CURREDITENDFUTURE-FAMILY, Curreditendfuture-given";
    public static final Map<String, String> CURREDITENDFUTURE_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, CURREDITENDFUTURE_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "curreditendfuture-1");
        put(PANEL_LABEL_SECONDARY_ID, "curreditendfuture-2");
        put(PANEL_LABEL_TERTIARY_ID, "curreditendfuture-3");
        put(PANEL_LABEL_DOB, "11-Nov-2011");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "012345555555");
    }};
    public static final String[][] CURREDITENDFUTURE_EPISODE = {{"02-Jun-2018 - N/A",SET_DOTS,"Discitis/vertebral osteomyelitis - metalwork"}};
    public static final String[][] CURREDITENDFUTURE_FUTURE_END = {{"02-Jun-2018 - 09-Oct-2019","495","Discitis/vertebral osteomyelitis - metalwork"}};

    public static final String CURRENT_NAME = "CURRENT-FAMILY, Current-given";
    public static final Map<String, String> CURRENT_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, CURRENT_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "current-1");
        put(PANEL_LABEL_SECONDARY_ID, "current-2");
        put(PANEL_LABEL_TERTIARY_ID, "current-3");
        put(PANEL_LABEL_DOB, "02-Feb-2002");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234111111");
    }};
    public static final String[][] CURRENT_EPISODE_SUMMARY = {{"01-Aug-2018 - N/A",SET_DOTS,"Congenital CMV"}};
    public static final String[][] CURRENT_MEDICATION_SUMMARY = {{"Aztreonam","28-Jul-2018"}};
    public static final String[][] CURRENT_REACTION_SUMMARY = {{"Other","Drug","02-Aug-2018"}};

    public static final String EDITREACTION_NAME = "EDITREACTION-FAMILY, Editreaction-given";
    public static final Map<String, String> EDITREACTION_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, EDITREACTION_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "editreaction-1");
        put(PANEL_LABEL_SECONDARY_ID, "editreaction-2");
        put(PANEL_LABEL_TERTIARY_ID, "editreaction-3");
        put(PANEL_LABEL_DOB, "05-May-2005");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234000002");
    }};
    public static final String[] EDITREACTION_REACTION_BEFORE = {"Diarrhoea (antibiotic induced)","Drug","08-Aug-2018"};
    public static final String[] EDITREACTION_REACTION_AFTER = {"Line occlusion","Line","21-Aug-2018"};
    public static final String[] EDITREACTION_TYPE_ONLY_BEFORE = {"Other","Drug","21-Aug-2018"};
    public static final String[] EDITREACTION_TYPE_ONLY_AFTER = {"Other","Line","21-Aug-2018"};

    public static final String EDITMEDICATION_NAME = "EDITMEDICATION-FAMILY, Editmedication-given";
    public static final Map<String, String> EDITMEDICATION_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, EDITMEDICATION_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "editmedication-1");
        put(PANEL_LABEL_SECONDARY_ID, "editmedication-2");
        put(PANEL_LABEL_TERTIARY_ID, "editmedication-3");
        put(PANEL_LABEL_DOB, "05-May-2005");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234000099");
    }};
    public static final String[] EDITMEDICATION_MEDICATION_CANDIDATE = {EDITMEDICATION_NAME,"Streptomycin"};
    public static final String[] EDITMEDICATION_MEDICATION_BEFORE = {"Streptomycin", "01-Oct-2018"};
    public static final String[] EDITMEDICATION_MEDICATION_AFTER_LIST = {"02-Oct-2018", "10-Oct-2018", "Flucloxacillin",};

    public static final String PAST_NAME = "PAST-FAMILY, Past-given";
    public static final Map<String, String> PAST_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, PAST_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "past-1");
        put(PANEL_LABEL_SECONDARY_ID, "past-2");
        put(PANEL_LABEL_TERTIARY_ID, "past-3");
        put(PANEL_LABEL_DOB, "02-Feb-2002");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234666666");
    }};
    public static final String[][] PAST_EPISODE = {{SET_DATES,"15","Aspergillosis"}};
    public static final int PAST_EPISODE_BED_DAYS = 3;
    public static final int PAST_EPISODE_HOURS = 7;

    public static final String PASTEDIT_NAME = "PASTEDIT-FAMILY, Pastedit-given";
    public static final Map<String, String> PASTEDIT_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, PASTEDIT_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "pastedit-1");
        put(PANEL_LABEL_SECONDARY_ID, "pastedit-2");
        put(PANEL_LABEL_TERTIARY_ID, "pastedit-3");
        put(PANEL_LABEL_DOB, "05-May-1995");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234777777");
    }};
    public static final String[][] PASTEDIT_EPISODE = {{"01-Mar-2018 - 04-Apr-2018","4","Bronchiectasis"}};
    public static final int PASTEDIT_EPISODE_BED_DAYS = 3;
    public static final int PASTEDIT_EPISODE_HOURS = 10;

    public static final String FUTUREADDMED_NAME = "FUTUREADDMED-FAMILY, Futureaddmed-given";
    public static final Map<String, String> FUTUREADDMED_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, FUTUREADDMED_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "futureaddmed-1");
        put(PANEL_LABEL_SECONDARY_ID, "futureaddmed-2");
        put(PANEL_LABEL_TERTIARY_ID, "futureaddmed-3");
        put(PANEL_LABEL_DOB, "11-Nov-2011");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234333333");
    }};
    public static final String[][] FUTUREADDMED_EPISODE_SUMMARY = {{
            "01-Jun-2019 - N/A",
            "",
            "Congenital CMV",
            ""}}; // todo add for all episodes? table decision time
    public static final String[][] FUTUREADDMED_MEDICATIONS_SUMMARY = {{
            "Ofloxacin",
            "02-Jul-2019"}};
    public static final String[][] FUTUREADDMED_MEDICATIONS_LIST = {{
            "02-Jul-2019",
            "",
            "Ofloxacin"}};

    public static final String FUTURE_NAME = "FUTURE-FAMILY, Future-given";
    public static final Map<String, String> FUTURE_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, FUTURE_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "future-1");
        put(PANEL_LABEL_SECONDARY_ID, "future-2");
        put(PANEL_LABEL_TERTIARY_ID, "future-3");
        put(PANEL_LABEL_DOB, "03-Mar-2003");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234888888");
    }};

    public static final String[][] FUTURE_EPISODE_SUMMARY = {{
            "01-Dec-2018 - 20-Dec-2018",
            "20",
            "Congenital CMV",
    ""}}; // todo add for all episodes? table decision time
    public static final String[][] FUTURE_MEDICATIONS_SUMMARY = {{
            "Flucloxacillin",
            "28-Jul-2018"}};
    public static final String[][] FUTURE_REACTIONS_SUMMARY = {{
            "Other",
            "Line",
            "02-Aug-2018"}};
    public static final String[][] FUTURE_REACTIONS_LIST = {{
            "02-Aug-2018",
            "Line",
            "Other"}};

    public static final String FUTURECANCEL_NAME = "FUTURECANCEL-FAMILY, Futurecancel-given";
    public static final Map<String, String> FUTURECANCEL_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, FUTURECANCEL_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "futurecancel-1");
        put(PANEL_LABEL_SECONDARY_ID, "futurecancel-2");
        put(PANEL_LABEL_TERTIARY_ID, "futurecancel-3");
        put(PANEL_LABEL_DOB, "03-Mar-2003");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234888888");
    }};

    public static final String[][] FUTURECANCEL_EPISODE_SUMMARY = {{
            "01-Dec-2018 - 20-Dec-2018",
            "20",
            "Congenital CMV",
            ""}}; // todo add for all episodes? table decision time
    public static final String[][] FUTURECANCEL_MEDICATIONS_SUMMARY = {{
            "Flucloxacillin",
            "28-Jul-2018"}};
    public static final String[][] FUTURECANCEL_REACTIONS_SUMMARY = {{
            "Other",
            "Line",
            "02-Aug-2018"}};
    public static final String[][] FUTURECANCEL_REACTIONS_LIST = {{
            "02-Aug-2018",
            "Line",
            "Other"}};

    public static final String PREVIOUS_NAME = "PREVIOUS-FAMILY, Previous-given";
    public static final Map<String, String> PREVIOUS_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, PREVIOUS_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "previous-1");
        put(PANEL_LABEL_SECONDARY_ID, "previous-2");
        put(PANEL_LABEL_TERTIARY_ID, "previous-3");
        put(PANEL_LABEL_DOB, "04-Apr-2004");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234999999");
    }};
    public static final String[] PREVIOUS_EPISODE_LIST = {
            "01-Jul-2018",
            "21-Jul-2018",
            "21", // DOTs
            "Improved", // Infection outcome
            "Partial success", // OPAT outcome
            "Bacterial meningitis"};
    public static final String[][] PREVIOUS_EPISODE_DETAIL = {{
            "Bacterial meningitis",
            "01-Jul-2018 - 21-Jul-2018",
            "12", // WTEs
            "No", // Readmitted
            "Improved", // Infection outcome
            "Partial success"}}; // OPAT outcome
    public static final String[] PREVIOUS_MEDICATION_DETAIL = {
            "28-Jun-2018",
            "21-Jul-2018",
            "24", // DOTs
            "Colistin",
            "Intravenous",
            "3", // Frequency
            "mg",
            "50"};
    public static final String[] PREVIOUS_MEDICATION_LIST_SUMMARY = {
            "28-Jun-2018",
            "21-Jul-2018",
            "Colistin"};
    public static final String[] PREVIOUS_REACTION_SUMMARY = {
            "Other",
            "Line",
            "02-Aug-2018"};

    public static final String UNSUITABLE_NAME = "UNSUITABLE-FAMILY, Unsuitable-given";
    public static final Map<String, String> UNSUITABLE_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, UNSUITABLE_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "unsuitable-1");
        put(PANEL_LABEL_SECONDARY_ID, "unsuitable-2");
        put(PANEL_LABEL_TERTIARY_ID, "unsuitable-3");
        put(PANEL_LABEL_DOB, "05-May-2005");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "0123456789");
    }};

    public static final String[] SUITABLE_CHECKS_SUMMARY = {
            "Standard",
            "Suitable",
            "<set-date>"};
    public static final String[] UNSUITABLE_CHECKS_SUMMARY = {
            "Standard",
            "Not suitable",
            "<set-date>"
    };

    public static final String MULTILINE_NAME = "MULTILINE-FAMILY, Multiline-given";
    public static final Map<String, String> MULTILINE_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, MULTILINE_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "multiline-1");
        put(PANEL_LABEL_DOB, "01-Jun-1965");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "01234000003");
    }};
    public static final String[] MULTILINE_EPISODES_SUMMARY = {
            "01-Jan-2018 - 20-Jan-2018-01-20",
            "20",
            "Wound Infection - post surgical"};

    public static final String[][] MULTILINE_TIMELINE_SUMMARY = {
            {
                TIMELINE_EPISODE_ENDED,
                "Infection outcome: Improved, OPAT outcome: Partial success",
                "20-Jan-2018",
                ""
            },
            {
                TIMELINE_EPISODE_STARTED,
                "Wound Infection - post surgical",
                "01-Jan-2018",
                ""
            },
            {
                TIMELINE_MEDICATION_ENDED,
                "Voriconazole",
                "20-Jan-2018",
                ""
            },
            {
                TIMELINE_CHECK,
                "Suitable",
                "01-Jan-2018",
                ""
            },
            {
                TIMELINE_REACTION,
                "Other (Line)",
                "02-Jan-2018",
                ""

            }
    };

    public static final String[][] MULTILINE_TIMELINE_FIRST_LAST = {
            {
                    TIMELINE_EPISODE_ENDED,
                    "Infection outcome: Improved, OPAT outcome: Partial success",
                    "20-Jan-2018",
                    ""
            },
            {
                    TIMELINE_MEDICATION_ENDED,
                    "Voriconazole",
                    "20-Jan-2018",
                    ""
            },
            {
                    TIMELINE_REACTION,
                    "Other (Line)",
                    "02-Jan-2018",
                    ""

            },
            {
                    TIMELINE_CHECK,
                    "Suitable",
                    "01-Feb-2009",
                    ""
            },
            {
                    TIMELINE_EPISODE_STARTED,
                    "Bacteraemia / blood stream infection/ Septicaemia",
                    "01-Feb-2009",
                    ""
            },
            {
                    TIMELINE_MEDICATION_STARTED,
                    "Piperacillin/Tazobactam",
                    "01-Feb-2009",
                    ""
            },
            {
                    TIMELINE_REACTION,
                    "Diarrhoea (antibiotic induced) (Drug)",
                    "02-Feb-2009",
                    ""

            }
    };

    public static final String[][] MULTILINE_EPISODES_LIST = {
            {
                "01-Jan-2018",
                "20-Jan-2018",
                "20",
                "Improved",
                "Partial success",
                "Wound Infection - post surgical"
            },{
                "01-Nov-2017",
                "20-Nov-2017",
                "20",
                "Improved",
                "Partial success",
                "VP Shunt Infection"
            },{
                "01-Oct-2016",
                "20-Oct-2016",
                "20",
                "Failure",
                "Failure",
                "Viral infection"
            },{
                "01-Sep-2015",
                "20-Sep-2015",
                "20",
                "Cured",
                "Success",
                "Vascular graft infection"
            },{
                "01-Aug-2014",
                "20-Aug-2014",
                "20",
                "Improved",
                "Failure",
                "Urinary tract infection"
            },{
                "01-Jul-2013",
                "20-Jul-2013",
                "20",
                "Improved",
                "Partial success",
                "Tuberculosis"
            },{
                "01-Jun-2012",
                "20-Jun-2012",
                "20",
                "Failure",
                "Partial success",
                "Toxic shock syndrome"
            },{
                "01-May-2011",
                "20-May-2011",
                "20",
                "Improved",
                "Partial success",
                "Tonsillitis"
            },{
                "01-Apr-2010",
                "20-Apr-2010",
                "20",
                "Cured",
                "Success",
                "Syphilis (Neuro)"
            },{
                "01-Dec-2009",
                "20-Dec-2009",
                "20",
                "Cured",
                "Success",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-Nov-2009",
                "20-Nov-2009",
                "20",
                "Improved",
                "Partial success",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-Oct-2009",
                "20-Oct-2009",
                "20",
                "Improved",
                "Partial success",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-Sep-2009",
                "20-Sep-2009",
                "20",
                "Improved",
                "Failure",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-Aug-2009",
                "20-Aug-2009",
                "20",
                "Improved",
                "Partial success",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-Jul-2009",
                "20-Jul-2009",
                "20",
                "Failure",
                "Not Determined",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-Jun-2009",
                "20-Jun-2009",
                "20",
                "Improved",
                "Partial success",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-May-2009",
                "20-May-2009",
                "20",
                "Improved",
                "Partial success",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-Apr-2009",
                "20-Apr-2009",
                "20",
                "Failure",
                "Failure",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-Mar-2009",
                "20-Mar-2009",
                "20",
                "Improved",
                "Partial success",
                "Bacteraemia / blood stream infection/ Septicaemia"
            },{
                "01-Feb-2009",
                "20-Feb-2009",
                "20",
                "Improved",
                "Partial success",
                "Bacteraemia / blood stream infection/ Septicaemia"
            }};
    public static final String[][] MULTILINE_CHECKS_LIST = {
            {"Standard","Suitable","01-Jan-2018"},
            {"Standard","Suitable","01-Nov-2017"},
            {"Standard","Suitable","01-Oct-2016"},
            {"Standard","Suitable","01-Sep-2015"},
            {"Standard","Suitable","01-Aug-2014"},
            {"Standard","Suitable","01-Jul-2013"},
            {"Standard","Suitable","01-Jun-2012"},
            {"Standard","Suitable","01-May-2011"},
            {"Standard","Suitable","01-Apr-2010"},
            {"Standard","Not suitable","01-Dec-2009"},
            {"Standard","Suitable","01-Nov-2009"},
            {"Standard","Suitable","01-Oct-2009"},
            {"Standard","Suitable","01-Sep-2009"},
            {"Standard","Suitable","01-Aug-2009"},
            {"Standard","Suitable","01-Jul-2009"},
            {"Standard","Suitable","01-Jun-2009"},
            {"Standard","Suitable","01-May-2009"},
            {"Standard","Suitable","01-Apr-2009"},
            {"Standard","Not suitable","01-Mar-2009"},
            {"Standard","Suitable","01-Feb-2009"}};


    public static final String[][] MULTILINE_MEDICATIONS_LIST = {
            {
                "01-Jan-2018",
                "20-Jan-2018",
                "Voriconazole"
            },{
                "01-Nov-2017",
                "20-Nov-2017",
                "Vancomycin"
            },{
                "01-Oct-2016",
                "20-Oct-2016",
                "Trimethoprim"
            },{
                "01-Sep-2015",
                "20-Sep-2015",
                "Tobramycin"
            },{
                "01-Aug-2014",
                "20-Aug-2014",
                "Tigcycline"
            },{
                "01-Jul-2013",
                "20-Jul-2013",
                "Tetracycline"
            },{
                "01-Jun-2012",
                "20-Jun-2012",
                "Temocillin"
            },{
                "01-May-2011",
                "20-May-2011",
                "Telavancin"
            },{
                "01-Apr-2010",
                "20-Apr-2010",
                "Teicoplanin"
            },{
                "01-Dec-2009",
                "20-Dec-2009",
                "Streptomycin"
            },{
                "01-Nov-2009",
                "20-Nov-2009",
                "Rifampicin"
            },{
                "01-Oct-2009",
                "20-Oct-2009",
                "Rifabutin"
            },{
                "01-Sep-2009",
                "20-Sep-2009",
                "Prothionamide"
            },{
                "01-Aug-2009",
                "20-Aug-2009",
                "Posaconazole"
            },{
                "01-Jul-2009",
                "20-Jul-2009",
                "Pivmecillinam"
            },{
                "01-Jun-2009",
                "20-Jun-2009",
                "Piperacillin/Tazobactam"
            },{
                "01-May-2009",
                "20-May-2009",
                "Piperacillin/Tazobactam"
            },{
                "01-Apr-2009",
                "20-Apr-2009",
                "Piperacillin/Tazobactam"
            },{
                "01-Mar-2009",
                "20-Mar-2009",
                "Piperacillin/Tazobactam"
            },{
                "01-Feb-2009",
                "20-Feb-2009",
                "Piperacillin/Tazobactam"
            }};

    public static final String[] MULTILINE_MEDICATIONS_SUMMARY = {"01-Jan-2018","20-Jan-2018","Voriconazole 50mg QDS"};
    public static final String[][] MULTILINE_MEDICATIONS_SHOW = {{"Drug", "Route", "Period", "Days of Therapy"},
                                                                {"Voriconazole 50mg QDS", "Intravenous","01-Jan-2018 - 20-Jan-2018",""}};

    public static final String[][] MULTILINE_REACTIONS_LIST = {
            {
                "02-Jan-2018",
                "Line",
                "Other"
            },{
                "02-Nov-2017",
                "Drug",
                "Other"
            },{
                "02-Oct-2016",
                "Line",
                "Thrombus"
            },{
                "02-Sep-2015",
                "Drug",
                "Rash"
            },{
                "02-Aug-2014",
                "Line",
                "Line occlusion"
            },{
                "02-Jul-2013",
                "Drug",
                "Hepatitis"
            },{
                "02-Jun-2012",
                "Line",
                "Line migration"
            },{
                "02-May-2011",
                "Drug",
                "Diarrhoea (antibiotic induced)"
            },{
                "02-Apr-2010",
                "Line",
                "Infections"
            },{
                "02-Dec-2009",
                "Drug",
                "C. diff"
            },{
                "02-Nov-2009",
                "Line",
                "Allergy to dressing"
            },{
                "02-Oct-2009",
                "Drug",
                "Blood dyscrasia"
            },{
                "02-Sep-2009",
                "Drug",
                "Diarrhoea (antibiotic induced)"
            },{
                "02-Aug-2009",
                "Drug",
                "Diarrhoea (antibiotic induced)"
            },{
                "02-Jul-2009",
                "Drug",
                "Diarrhoea (antibiotic induced)"
            },{
                "02-Jun-2009",
                "Drug",
                "Diarrhoea (antibiotic induced)"
            },{
                "02-May-2009",
                "Drug",
                "Diarrhoea (antibiotic induced)"
            },{
                "02-Apr-2009",
                "Drug",
                "Diarrhoea (antibiotic induced)"
            },{
                "02-Mar-2009",
                "Drug",
                "Diarrhoea (antibiotic induced)"
            },{
                "02-Feb-2009",
                "Drug",
                "Diarrhoea (antibiotic induced)"
            }};

    public static final String[] MULTILINE_REACTIONS_SUMMARY = {"02-Aug-2014","Line","Line occlusion"};
    public static final String[][] MULTILINE_REACTIONS_SHOW = {{"Condition", "Date"},
                                                                {"Line occlusion (Line)", "02-Aug-2014"}};
    /*
     * this is a calculation of the number of elements that should be viewed on the
     * timeline, rather than checking every ticket the test check the first and last few
     * plus a count of how many are expected
     */
    public static final int MULTILINE_TIMELINE_COUNT =
            (MULTILINE_EPISODES_LIST.length+MULTILINE_MEDICATIONS_LIST.length)*2
                    +MULTILINE_REACTIONS_LIST.length
                    +MULTILINE_CHECKS_LIST.length;

    /*
     * these is to count the adverse reactions after filtering the timeline
     */
    public static final int MULTILINE_TIMELINE_REACTIONS_COUNT =
            MULTILINE_REACTIONS_LIST.length;

    public static final int MULTILINE_TIMELINE_MEDICATIONS_COUNT =
            (MULTILINE_MEDICATIONS_LIST.length * 2);

    public static final int MULTILINE_TIMELINE_EPISODES_COUNT =
            (MULTILINE_EPISODES_LIST.length * 2);

    public static final int MULTILINE_TIMELINE_CHECKS_COUNT =
            MULTILINE_CHECKS_LIST.length;

    public static final String[] INITIAL_CANDIDATE_LIST = {
            ADDREACTION_NAME,
            CANDADD_NAME,
            CANDADDCHECK_NAME,
            CANDEDIT_NAME,
            CANDIDATE_NAME,
            EDITMEDICATION_NAME,
            EDITREACTION_NAME,
            MULTILINE_NAME,
            NOTE_NAME,
            PREVIOUS_NAME,
            UNSUITABLE_NAME
    };

    public static final String[] INITIAL_PATIENT_LIST = {
            CURRADDMED_NAME,
            CURRCANCEL_NAME,
            CURREDITEND_NAME,
            CURREDITENDFUTURE_NAME,
            CURRENT_NAME,
            FUTURE_NAME,
            FUTUREADDMED_NAME,
            FUTURECANCEL_NAME,
            PAST_NAME,
            PASTEDIT_NAME
    };

    /**
     * First Element is the expected result
     * 1 = Pass
     * 0 = Fail
     * Questionnaire answers
     * 0 = Yes
     * 1 = No
     * 2 = Unknown
     */
    public static final int[] SUITABLE_ANSWER_1 = {1,0,1,0,0,0,0,0,1};
    public static final int[] UNSUITABLE_ANSWER_1 = {0,1,1,0,0,0,0,0,0};
    public static final int[] MISSING_ANSWER_1 = {1,0,1,0,0,0,0,1};

    /** List of all answers for each question when others are valid **/
    public static final int[][] ALL_ANSWERS = {
            // correct
            {1,0,1,0,0,0,0,0,2},

            // alter Q1
            {0,1,1,0,0,0,0,0,2},
            {0,2,1,0,0,0,0,0,2},

            // alter Q2
            {0,0,0,0,0,0,0,0,2},
            {1,0,2,0,0,0,0,0,2},

            // alter Q3
            {0,0,1,1,0,0,0,0,2},
            {0,0,1,2,0,0,0,0,2},

            // alter Q4
            {0,0,1,0,1,0,0,0,2},
            {0,0,1,0,2,0,0,0,2},

            // alter Q5
            {0,0,1,0,0,1,0,0,2},
            {0,0,1,0,0,2,0,0,2},

            // alter Q6
            {0,0,1,0,0,0,1,0,2},
            {0,0,1,0,0,0,2,0,2},

            // alter Q7
            {0,0,1,0,0,0,0,1,2},
            {1,0,1,0,0,0,0,2,2},

            // alter Q7
            {1,0,1,0,0,0,0,0,0},
            {1,0,1,0,0,0,0,0,1},
    };

    /**
     * Workflow statics for data checking
     */
    public static final String STORY_ADD_NAME = "ADD-FAMILY, Add-given";
    public static final Map<String, String> STORY_ADD_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, STORY_ADD_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "Add-Prime");
        put(PANEL_LABEL_TERTIARY_ID, "Add-Third");
        put(PANEL_LABEL_DOB, "01-Jan-1980");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "(+44) 1254 999999");
    }};
    public static final String[][] STORY_ADD_EPISODE_PORTLET = {{"09-Aug-2018 - N/A",SET_DOTS,"Cerebral abscess"}};
    public static final String[][] STORY_END_EPISODE_LIST = {{"09-Aug-2018","20-Aug-2018","12","Cured","Success","Cerebral abscess"}};
    public static final String[][] STORY_EDIT_EPISODE_DETAIL = {{
            "Cerebral abscess",
            "09-Aug-2018 - 20-Aug-2018",
            "10", // WTEs
            "No", // Readmitted
            "Cured", // Infection outcome
            "Success"}}; // OPAT outcome
    public static final String[][] STORY_ADD_MEDICATION_PORTLET = {{"Flucloxacillin","22-Jun-2018"}};

    // STORY_ADD_NAME changes to STORY_EDIT_NAME
    public static final String STORY_EDIT_NAME = "EDIT-FAMILY, Edit-given";
    public static final Map<String, String> STORY_EDIT_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, STORY_EDIT_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "Edit-Prime");
        put(PANEL_LABEL_SECONDARY_ID, "Edit-Second");
        put(PANEL_LABEL_DOB, "01-Jan-2000");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "01254111111");
    }};
    public static final Map<String, String> STORY_EDIT_EXTRA_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_SECONDARY_PHONE, "01254222222");
        put(PANEL_LABEL_EMAIL, "patient@email.domain");
        put(PANEL_LABEL_CARER_NAME, "Edit Carer");
        put(PANEL_LABEL_CARER_PHONE, "01254333333");
        put(PANEL_LABEL_CARER_RELATIONSHIP, "The Good Son");
    }};

    public static final String STORY_ADD_PATIENT_ONLY_NAME = "ONLY-FAMILY, Only-given";
    public static final Map<String, String> STORY_ADD_PATIENT_ONLY_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, STORY_ADD_PATIENT_ONLY_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "Only-Prime");
        put(PANEL_LABEL_SECONDARY_ID, "Only-Second");
        put(PANEL_LABEL_TERTIARY_ID, "Only-Third");
        put(PANEL_LABEL_DOB, "01-Jan-1960");
        put(PANEL_LABEL_SEX, "Female");
        put(PANEL_LABEL_PRIMARY_PHONE, "0113 111111");
    }};
    public static final Map<String, String> STORY_ADD_PATIENT_ONLY_EXTRA_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_SECONDARY_PHONE, "(0113) 222222");
        put(PANEL_LABEL_EMAIL, "onlypatient@email.domain");
        put(PANEL_LABEL_CARER_NAME, "Only Carer");
        put(PANEL_LABEL_CARER_PHONE, "0113333333");
        put(PANEL_LABEL_CARER_RELATIONSHIP, "Only Son");
    }};
    public static final String[][] STORY_PATIENT_ONLY_CURR_MEDICATION_PORTLET = {{"Cefuroxime","05-Sep-2018"}};
    public static final String[] STORY_PATIENT_ONLY_CURR_MEDICATION_LIST_SUMMARY = {
            "05-Sep-2018",
            "",
            "Cefuroxime"};
    public static final String[][] STORY_PATIENT_ONLY_ADD_EPISODE_PORTLET = {{"04-Sep-2018 - N/A",SET_DOTS,"Empyema"}};
    public static final String[][] STORY_PATIENT_ONLY_END_EPISODE_LIST = {{"04-Sep-2018","07-Sep-2018","4","Improved","Partial success","Empyema"}};

    public static final String STORY_ADD_PATIENT_MED_NAME = "MED-FAMILY, Med-given";
    public static final Map<String, String> STORY_ADD_PATIENT_MED_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_NAME, STORY_ADD_PATIENT_MED_NAME);
        put(PANEL_LABEL_PRIMARY_ID, "Med-Prime");
        put(PANEL_LABEL_SECONDARY_ID, "Med-Second");
        put(PANEL_LABEL_TERTIARY_ID, "Med-Third");
        put(PANEL_LABEL_DOB, "01-Jan-1961");
        put(PANEL_LABEL_SEX, "Male");
        put(PANEL_LABEL_PRIMARY_PHONE, "07767 111111");
    }};
    public static final Map<String, String> STORY_ADD_PATIENT_MED_EXTRA_SUMMARY = new LinkedHashMap<String,String>()
    {{
        put(PANEL_LABEL_SECONDARY_PHONE, "07767 222222");
        put(PANEL_LABEL_EMAIL, "medspatient@email.domain");
        put(PANEL_LABEL_CARER_NAME, "Meds Carer");
        put(PANEL_LABEL_CARER_PHONE, "+44(0)7767333333");
        put(PANEL_LABEL_CARER_RELATIONSHIP, "Meds Son");
    }};
    public static final String[] STORY_ADD_PATIENT_MED_MEDICATION_PORTLET = {"Amikacin","12-Dec-1990"};
    public static final String[][] STORY_ADD_PATIENT_MED_MEDICATION_LIST_SUMMARY = {{
            "01-Dec-1990",
            "14-Feb-1991",
            "Amikacin"}};
    public static final String[][] STORY_PATIENT_MED_CURR_MEDICATION_PORTLET = {{"Amikacin","01-Sep-2018"}};
    public static final String[] STORY_PATIENT_MED_CURR_MEDICATION_LIST_SUMMARY = {
            "01-Sep-2018",
            "",
            "Amikacin"};
    public static final String[][] STORY_PATIENT_MED_ADD_EPISODE_PORTLET = {{"03-Sep-2018 - N/A",SET_DOTS,"Bacterial meningitis"}};
    public static final String[][] STORY_PATIENT_MED_END_EPISODE_LIST = {{"03-Sep-2018","06-Sep-2018","4","Failure","Failure","Bacterial meningitis"}};
    public static String[] STORY_PATIENT_MED_CHECK = {"Standard","Suitable","<today>"};

    public static final String[] STORY_DRUG_REACTION_ONE = {"Diarrhoea (antibiotic induced)","Drug","07-Aug-2018"};
    public static final String[] STORY_DRUG_REACTION_TWO = {"Hepatitis","Drug","07-Aug-2018"};
    public static final String[] STORY_LINE_REACTION = {"Allergy to dressing","Line","08-Aug-2018"};

    /**
     * Get current date in given format
     * @param _datePattern pattern to use
     * @return string representing today's date in the given pattern
     */
    public static String getToday(String _datePattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(_datePattern);
        return LocalDate.now().format(formatter);
    }

    /**
     * Get date equal to today modified by a number
     * of days, in given pattern
     * @param _datePattern pattern to use
     * @param _dayModifier number of days to modify by
     * @return string representing the date from today with number of days difference
     */

    public static String getToday(String _datePattern, int _dayModifier) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(_datePattern);
        return LocalDate.now().plusDays(_dayModifier).format(formatter);
    }

    /**
     * Calculate the number of days from a date to today inclusive
     * Used for DOTs etc
     * @param _datePattern pattern of date to check
     * @param _dateFrom date to check
     * @return string type of number of days
     */
    public static String daysToToday(String _datePattern, String _dateFrom){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(_datePattern);

        LocalDate dateStart = LocalDate.parse(_dateFrom, formatter);
        System.out.println(DAYS.between(dateStart, LocalDate.now()));
        return Long.toString((DAYS.between(dateStart, LocalDate.now())) + 1);
    }

    public static final String DATE_DISPLAY_FORMAT = "dd-MMM-yyyy";
    public static final String DATE_ENTRY_FORMAT = "yyyy-MM-dd";
    public static final String [][] EMPTY_PORTLET = {{""}};
}

