package com.baxter.seleniumopat;

import com.baxter.seleniumopat.utilities.Config;
import com.baxter.seleniumopat.utilities.FormHandler;
import com.baxter.seleniumopat.utilities.TestLogger;
import com.baxter.seleniumopat.utilities.driver.WaitingRemoteWebDriver;
import com.baxter.seleniumopat.utilities.rules.TestPrintHandler;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import org.junit.*;
import org.junit.runners.Parameterized;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

abstract public class BaseParallelTest {

    /**
     *
     */
    protected WaitingRemoteWebDriver driver;

    /**
     *
     */
    private String baseurl;

    /**
     * Configuration needed for the tests
     */
    protected static Config config;

    /**
     * Logger
     */
    protected static Logger logger;

    /**
     *
     */
    protected static FormHandler formHandler;

    /**
     *
     */
    protected BrowserTarget browserTarget;

    /**
     *
     */
    private static List<BrowserTarget> browserTargets = new ArrayList<>();

    /**
     *
     */
    static private List<WaitingRemoteWebDriver> drivers = new ArrayList<>();

    /**
     * This rule intercepts failed unit tests before JUnit sends out the
     * results. The idea is that the callback TestPrintHandler can extract
     * only useful information from the exception and export it to the logs.
     */
    @Rule
    public TestPrintHandler failure = new TestPrintHandler(logger, config);

    /**
     * initialise config and logger classes;
     * hold all Configuration values in a LinkedList;
     *
     * @return
     * @throws Exception
     */
    @Parameterized.Parameters
    public static LinkedList<BrowserTarget[]> init() {

        // initialize log
        initLogger();

        // load configurations needed for the tests
        initConfig();

        // defaultTestTarget is the default configuration file for local tests
        String defaultTestTarget = config.getProperty("test-targets");
        if (defaultTestTarget.equals(""))
            defaultTestTarget = "local-targets.json";
        browserTargets = getBrowserTargets(defaultTestTarget);

        LinkedList<BrowserTarget[]> env = new LinkedList<>();
        for (BrowserTarget bTarget : browserTargets) {

            env.add(new BrowserTarget[]{bTarget});
        }

        return env;
    }

    /**
     * constructor
     *
     * @param browserTarget
     */
    public BaseParallelTest(BrowserTarget browserTarget) {

        this.browserTarget = browserTarget;
    }

    @Before
    public void setUp() throws MalformedURLException, InterruptedException {

        // load configurations needed for the tests
        initConfig();

        driver = new WaitingRemoteWebDriver(getDriver(), 10);
        failure.setDriver(driver);
        initFormHandler(driver);

        /**
         * sometimes, exception is thrown because window fails to maximize(Chrome only); adding a delay before
         * window.maximize seems to solve the issue.
         * @see https://bugs.chromium.org/p/chromedriver/issues/detail?id=1901
         */
        TimeUnit.MILLISECONDS.sleep(300);

        driver.setBaseUrl(baseurl);
        driver.manage().window().maximize();
        drivers.add(driver);
    }

    /**
     * perform logout and quit driver, so any busy grid nodes can be reused(if using selenium-grid)
     */
    @After
    public void tearDownAfterTestsBaseClass() {

         formHandler = null;
    }

    /**
     * Creates a RemoteWebDriver according to the configuration required from config.properties::test-targets;
     * Default execution is local and using ChromeDriver.
     *
     * @return RemoteWebDriver
     * @throws MalformedURLException
     */
    private RemoteWebDriver getDriver() throws MalformedURLException {

        // set DesiredCapabilities
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("browserName", browserTarget.browserName);
        capabilities.setCapability("os", browserTarget.os);
        capabilities.setCapability("os_version", browserTarget.osVersion);
        capabilities.setCapability("build", "PPS");

        RemoteWebDriver remoteWebDriver = null;
        baseurl = config.getProperty("baseurl");

        // select run configuration for the tests
        switch (config.getProperty("test-targets")) {

            case "jenkins-headless-targets.json":
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--window-size=1920,1080");
                chromeOptions.addArguments("--no-sandbox");
                chromeOptions.addArguments("--headless");
                remoteWebDriver = new RemoteWebDriver(new URL(config.getProperty("ci-server-seleniumgrid-url")), chromeOptions);
                baseurl = browserTarget.baseurl + ":" + browserTarget.containerPort;
                break;
            case "local-selenium-grid-targets.json":
                remoteWebDriver = new RemoteWebDriver(new URL(config.getProperty("local-seleniumgrid-url")), capabilities);
                break;
            case "jenkins-browserstack-targets.json":
                capabilities.setCapability("browserstack.local", true);
                capabilities.setCapability("browserstack.debug", true);
                remoteWebDriver = new RemoteWebDriver(new URL(config.getProperty("browserstack-url")), capabilities);
                baseurl = browserTarget.baseurl + ":" + browserTarget.containerPort;
                break;
            case "local-chrome.json":
                remoteWebDriver = new ChromeDriver();
                break;
            case "local-firefox.json":
                remoteWebDriver = new FirefoxDriver();
                break;
            case "local-ie.json":
                remoteWebDriver = new InternetExplorerDriver();
                break;
            default:
                // local-targets.json
                remoteWebDriver = new ChromeDriver();
                break;
        }

        return remoteWebDriver;
    }

    private static void initConfig() {
        if (config == null) {
            config = new Config();

            String browserDriverPath = config.getProperty("test-targets");
            if (browserDriverPath.equals("local-chrome.json")) {
                String driverPath = config.getProperty("chromedriverPath");
                if (driverPath != null && !driverPath.equals("")) {
                    System.setProperty("webdriver.chrome.driver", driverPath);
                }
            }
            else if(browserDriverPath.equals("local-firefox.json")) {
                String driverPath = config.getProperty("geckodriverPath");
                if (driverPath != null && !driverPath.equals("")) {
                    System.setProperty("webdriver.gecko.driver", driverPath);
                }
            }
            else if(browserDriverPath.equals("local-ie.json")) {
                String driverPath = config.getProperty("iedriverPath");
                if (driverPath != null && !driverPath.equals("")) {
                    System.setProperty("webdriver.ie.driver", driverPath);
                }
            }
        }
    }

    private static Logger initLogger() {
        if (logger == null) {
            //logger = new TestLogger(BaseTest.class.getSimpleName()).get();
            logger = new TestLogger(BaseParallelTest.class.getSimpleName()).get();
        }

        return logger;
    }

    private static FormHandler initFormHandler(WaitingRemoteWebDriver _driver) {
        if (formHandler == null) {
            formHandler = new FormHandler(_driver);
        }

        return formHandler;
    }

    /**
     * Load browser targets accordingly to the value set on the config.properties property "test-target"
     *
     * @param _testTargetsFile Test targets Script file name
     * @return List<WebElement> webElements
     */
    protected static List<BrowserTarget> getBrowserTargets(String _testTargetsFile) {

        ObjectMapper mapper = new ObjectMapper();

        mapper.addHandler(new DeserializationProblemHandler() {
            @Override
            public boolean handleUnknownProperty(DeserializationContext ctxt, JsonParser p, JsonDeserializer<?> deserializer, Object beanOrClass, String propertyName) throws IOException {
                return true;
            }
        });

        List<BrowserTarget> browserTargets = new ArrayList<BrowserTarget>();

        //JSON from file to Object
        try {

            // load test script file
            ClassLoader classLoader = FormHandler.class.getClassLoader();
            File scriptFile = new File(classLoader.getResource("conf/" + _testTargetsFile).getFile());

            // map browser targets into a list of BrowserTarget
            browserTargets = Arrays.asList(mapper.readValue(scriptFile, BrowserTarget[].class));
        } catch (java.io.IOException e) {

            e.printStackTrace();
        }

        return browserTargets;
    }

    protected static class BrowserTarget {

        public String browserName;
        public String browserVersion;
        public String os;
        public String osVersion;
        public String baseurl;
        public String containerPort;
        public String[] driverArguments;

        public BrowserTarget() {}
    }
}
