package com.baxter.seleniumopat.tests;

import com.baxter.seleniumopat.BaseParallelTestWithAuth;
import com.baxter.seleniumopat.pages.*;
import com.baxter.seleniumopat.utilities.LayoutHandler;
import com.baxter.seleniumopat.utilities.Parallelized;
import org.junit.Before;
import org.junit.runner.RunWith;

import java.util.Random;

/**
 * To use this test in an empty OPAT environment:
 *
 * Navigate to the host Homestead folder and:
 *
 * vagrant ssh
 * cd code/opat
 * php artisan migrate:refresh
 * php artisan db:seed --class=StaticDataSeeder
 */


@RunWith(Parallelized.class)
public class WizardTest extends BaseParallelTestWithAuth {
    AddressPage addressPage;
    ContactPage contactPage;
    EpisodePage episodePage;
    MedicationPage medicationPage;
    PatientPage patientPage;
    SummaryPage summaryPage;
    int startEpisodeNumber;
    // constructor
    public WizardTest(BrowserTarget browserTarget) {
        super(browserTarget);
    }

    /**
     * BASE CLASS FOR WORKFLOW WIZARD TESTS
     * Initialise pages and other resources
     */
    @Before
    public void setUpPages() {
        // Randomise entry point
        String button1Id;
        String button2Id;
        Random rand = new Random();
        int buttonUse = rand.nextInt(3);

        switch (buttonUse) {
            case 0:
                button1Id = DashboardPage.CANDIDATES_MENU;
                button2Id = ListCandidatesPage.NEW_PATIENT_ACTION;
                break;
            case 1:
                button1Id = DashboardPage.PATIENTS_MENU;
                button2Id = ListPatientsPage.NEW_PATIENT_ACTION;
                break;
            default:
                button1Id = DashboardPage.CANDIDATES_MENU;
                button2Id = ListCandidatesPage.NEW_PATIENT_ACTION;
        }
        System.out.println("Using button: " + button1Id);
        // Save number of current Episode
        DashboardPage dashboardPage = new DashboardPage(driver,logger);
        startEpisodeNumber = dashboardPage.getNumberCurrentEpisodes();

        // Navigate to Page
        driver.findElementById(button1Id).click();
        driver.findElementById(button2Id).click();

        patientPage = new PatientPage(driver, LayoutHandler.PageType.WIZARD);

        // Check page layout
        patientPage.checkPageLayout();
    }
}
