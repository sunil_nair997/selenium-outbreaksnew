package com.baxter.seleniumopat.tests;

import com.baxter.seleniumopat.BaseParallelTestWithAuth;
import com.baxter.seleniumopat.pages.DashboardPage;
import com.baxter.seleniumopat.utilities.Parallelized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Parallelized.class)
public class WorkflowInitialDashboardTest extends BaseParallelTestWithAuth {

    DashboardPage dashboardPage;

    // constructor
    public WorkflowInitialDashboardTest(BrowserTarget browserTarget) {
        super(browserTarget);
    }

    /**
     * Initialise pages and other resources
     */
    @Before
    public void setUpPages() {
        dashboardPage = new DashboardPage(driver,logger);
    }

    @Test
    public void checkDashboardLayoutTest() {
        dashboardPage.checkPageLayout(false,"0","0","0","0","0","0");
    }
}


