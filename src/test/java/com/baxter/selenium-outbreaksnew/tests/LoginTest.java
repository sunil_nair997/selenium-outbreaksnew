package com.baxter.seleniumopat.tests;

import com.baxter.seleniumopat.BaseParallelTest;
import com.baxter.seleniumopat.pages.LoginPage;
import com.baxter.seleniumopat.utilities.Parallelized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.TestCase.assertTrue;

/**
 * This class will perform login tests to be run once at the beginning of
 * each test Suite.
 *
 * Each test(@Test) must "throws Exception", so failed tests can be intercepted and treated accordingly.
 * @see com.baxter.seleniumopat.BaseTest @Rule TestPrintHandler failure
 */
@RunWith(Parallelized.class)
public class LoginTest extends BaseParallelTest {

    // constructor
    public LoginTest(BrowserTarget browserTarget) {
        super(browserTarget);
    }

    /**
     * LoginPage
     */
    private LoginPage loginPage;

    /**
     * Initialize page object pattern for LoginPage
     */
    @Before
    public void setUpBeforeTest() {

        loginPage = new LoginPage(driver);
    }

    @Test
    public void loginTest() {

        // try to login
        assertTrue(loginPage.login(config.getProperty("username"), config.getProperty("password")));
    }
}
