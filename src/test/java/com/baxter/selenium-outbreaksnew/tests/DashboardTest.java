package com.baxter.seleniumopat.tests;

import com.baxter.seleniumopat.BaseParallelTestWithAuth;
import com.baxter.seleniumopat.pages.DashboardPage;
import com.baxter.seleniumopat.utilities.LayoutHandler;
import com.baxter.seleniumopat.utilities.Parallelized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

@RunWith(Parallelized.class)
public class DashboardTest extends BaseParallelTestWithAuth {

    DashboardPage dashboardPage;

    // constructor
    public DashboardTest(BrowserTarget browserTarget) {
        super(browserTarget);
    }

    /**
     * Initialise pages and other resources
     */
    @Before
    public void setUpPages() {
        dashboardPage = new DashboardPage(driver,logger);
    }

    @Test
    public void checkDashboardLayoutTest() {
        dashboardPage.checkPageLayout();
        dashboardPage.logout();
        LayoutHandler layoutHandler = new LayoutHandler(driver);
        assertTrue("Logout - expected Keycloak form", layoutHandler.isIdPresent(LayoutHandler.KEYCLOAK_LOGIN_FORM_ID));
    }
}


