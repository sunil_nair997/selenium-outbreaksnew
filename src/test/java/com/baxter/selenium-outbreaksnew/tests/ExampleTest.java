package com.baxter.seleniumopat.tests;

import com.baxter.seleniumopat.BaseParallelTest;
import com.baxter.seleniumopat.utilities.Parallelized;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Parallelized.class)
public class ExampleTest extends BaseParallelTest {

    // constructor
    public ExampleTest(BaseParallelTest.BrowserTarget browserTarget) {
        super(browserTarget);
    }

    @Test
    public void testExample1() {

        assertTrue(true);
    }

    @Test
    public void testExample2() {

        assertTrue(true);
    }

    @Test
    public void testExample3() {

        assertTrue(true);
    }
}
